package Calc;

import org.junit.jupiter.api.AfterAll;

import org.junit.jupiter.api.BeforeAll;

import org.openqa.selenium.chrome.*;

public class Settings {

    protected String C = "[onclick=\"r('C')\"]";
    protected String zero = "[onclick='r(0)']";
    protected String one = "[onclick='r(1)']";
    protected String two = "[onclick='r(2)']";
    protected String three = "[onclick='r(3)']";
    protected String four = "[onclick='r(4)']";
    protected String five = "[onclick='r(5)']";
    protected String six = "[onclick='r(6)']";
    protected String seven = "[onclick='r(7)']";
    protected String eight = "[onclick='r(8)']";
    protected String nine = "[onclick='r(9)']";
    protected String point = "[onclick=\"r('.')\"]";
    protected String plus = "[onclick=\"r('+')\"]";
    protected String minus = "[onclick=\"r('-')\"]";
    protected String result = "[onclick=\"r('=')\"]";
    protected String plusMin = "[onclick=\"r('+/-')\"]";
    protected String mul = "[onclick=\"r('*')\"]";
    protected String div = "[onclick=\"r('/')\"]";
    protected String sin = "[onclick=\"r('sin')\"]";
    protected String cos = "[onclick=\"r('cos')\"]";
    protected String tan = "[onclick=\"r('tan')\"]";
    protected String sin_neg1 = "[onclick=\"r('asin')\"]";
    protected String cos_neg1 = "[onclick=\"r('acos')\"]";
    protected String tan_neg1 = "[onclick=\"r('atan')\"]";
    protected String pi = "[onclick=\"r('pi')\"]";
    protected String e = "[onclick=\"r('e')\"]";
    protected String pow = "[onclick=\"r('pow')\"]";
    protected String x3 = "[onclick=\"r('x3')\"]";
    protected String x2 = "[onclick=\"r('x2')\"]";
    protected String ex = "[onclick=\"r('ex')\"]";
    protected String tenX = "[onclick=\"r('10x')\"]";
    protected String apow = "[onclick=\"r('apow')\"]";
    protected String threeX = "[onclick=\"r('3x')\"]";
    protected String sqrt = "[onclick=\"r('sqrt')\"]";
    protected String Ln = "[onclick=\"r('ln')\"]";
    protected String log = "[onclick=\"r('log')\"]";
    protected String oneDivX = "[onclick=\"r('1/x')\"]";
    protected String pc = "[onclick=\"r('pc')\"]";
    protected String factorial = "[onclick=\"r('n!')\"]";

    public static ChromeDriver chrome;
    @BeforeAll
    public static void onSetUp () {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Ivan\\Desktop\\Driver\\chromedriver.exe");
        chrome = new ChromeDriver();
        chrome.get("http://34.90.36.71/apps/anrzhievskyi/scientificCalculator/index.html");
    }

    @AfterAll
    public static void close () {
        chrome.quit();
    }
}
