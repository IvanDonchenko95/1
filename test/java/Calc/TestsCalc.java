package Calc;



import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;


public class TestsCalc extends Settings {

    @Test
    public void test0_Nums_OK () {
        chrome.findElement(By.cssSelector(one)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(three)).click();
        chrome.findElement(By.cssSelector(four)).click();
        chrome.findElement(By.cssSelector(five)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(seven)).click();
        chrome.findElement(By.cssSelector(eight)).click();
        chrome.findElement(By.cssSelector(nine)).click();
        chrome.findElement(By.cssSelector(zero)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("1234567890",res);

    }
    @Test
    public void test1_neg_2_add_neg_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-4,rst);

    }
    @Test
    public void test2_neg_2_add_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-2,rst);
    }
    @Test
    public void test3_neg_2_add_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test4_0_add_neg_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-2,rst);
    }
    @Test
    public void test5_0_add_0_OK () {

        chrome.findElement(By.cssSelector("[onclick='r(0)']")).click();
        chrome.findElement(By.cssSelector("[onclick=\"r('+')\"]")).click();
        chrome.findElement(By.cssSelector("[onclick='r(0)']")).click();
        chrome.findElement(By.cssSelector("[onclick=\"r('=')\"]")).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector("[onclick=\"r('C')\"]")).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test6_0_add_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(2,rst);
    }
    @Test
    public void test7_2_add_neg_2_OK() {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test8_2_add_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(2,rst);
    }
    @Test
    public void test9_2_add_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(4,rst);
    }
    @Test
    public void test10_neg_2_minus_neg_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);

    }
    @Test
    public void test11_neg_2_minus_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-2,rst);
    }
    @Test
    public void test12_neg_2_minus_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-4,rst);
    }
    @Test
    public void test13_0_minus_neg_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(2,rst);
    }
    @Test
    public void test14_0_minus_0_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test15_0_minus_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-2,rst);
    }
    @Test
    public void test16_2_minus_neg_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(4,rst);
    }
    @Test
    public void test17_2_minus_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(2,rst);
    }
    @Test
    public void test18_2_minus_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test19_neg_2_multyply_neg_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(4,rst);

    }
    @Test
    public void test20_neg_2_multyply_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test21_neg_2_multyply_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-4,rst);
    }
    @Test
    public void test22_0_multyply_neg_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test23_0_multyply_0_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test24_0_multyply_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test25_2_multyply_neg_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(-4,rst);
    }
    @Test
    public void test26_2_multyply_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test27_2_multyply_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(mul)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(4,rst);
    }
    @Test
    public void test28_neg_2_div_neg_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(1,rst);

    }
    @Test
    public void test29_neg_2_div_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("Error",res);
    }
    @Test
    public void test30_neg_2_div_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();

        Assertions.assertEquals(-1,rst);
    }
    @Test
    public void test31_0_div_neg_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();

        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test32_0_div_0_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();

        Assertions.assertEquals("Error",res);
    }
    @Test
    public void test33_0_div_2_OK () {

        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();

        Assertions.assertEquals(0,rst);
    }
    @Test
    public void test34_2_div_neg_2_OK() {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(plusMin)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();

        Assertions.assertEquals(-1,rst);
    }
    @Test
    public void test35_2_div_0_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(zero)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();

        Assertions.assertEquals("Error",res);
    }
    @Test
    public void test36_2_div_2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(div)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(1,rst);
    }
    @Test
    public void test37_26_Sin_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(sin)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0.4383711468,rst);
    }
    @Test
    public void test38_26_Cos_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(cos)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();;
        Assertions.assertEquals(0.8987940463,rst);
    }
    @Test
    public void test39_26_Tan_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(tan)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(0.4877325886,rst);
    }
    @Test
    public void test40_26_add_PI_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(pi)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(29.141592654,rst);
    }
    @Test
    public void test41_26_min_PI_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(pi)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(22.858407346,rst);
    }
    @Test
    public void test42_26_add_E_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(plus)).click();
        chrome.findElement(By.cssSelector(e)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(28.718281828,rst);
    }
    @Test
    public void test42_26_min_E_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(minus)).click();
        chrome.findElement(By.cssSelector(e)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        double rst = Double.valueOf(res);

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(23.281718172,rst);
    }
    @Test
    public void test43_26_xY_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(pow)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(676,rst);
    }
    @Test
    public void test44_26_x3_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(x3)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(17576,rst);
    }
    @Test
    public void test45_26_x2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(x2)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        int rst = Integer.parseInt(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(676,rst);
    }
    @Test
    public void test46_26_eX_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(ex)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();
        StringBuilder sb = new StringBuilder(res);
        sb.deleteCharAt(sb.length()-1);
        long rst = Long.parseLong(String.valueOf(sb));

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals(195729609429L,rst);
    }
    @Test
    public void test47_26_tenX_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(tenX)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("9.999999999 +25",res);
    }
    @Test
    public void test48_Y26_X2_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(apow)).click();
        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(result)).click();
        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("5.0990195136",res);
    }
    @Test
    public void test49_26_3X_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(threeX)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();

        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("2.9624960684",res);
    }
    @Test
    public void test50_26_sqrt__OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(sqrt)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("5.0990195136",res);
    }
    @Test
    public void test51_26_ln__OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(Ln)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("3.258096538",res);
    }
    @Test
    public void test52_26_log__OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(log)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("1.414973348",res);
    }
    @Test
    public void test53_26_1_div_X_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(oneDivX)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("0.0384615385",res);
    }
    @Test
    public void test54_26_percent_Div_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(pc)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("0.26",res);
    }
    @Test
    public void test55_26_fact_OK () {

        chrome.findElement(By.cssSelector(two)).click();
        chrome.findElement(By.cssSelector(six)).click();
        chrome.findElement(By.cssSelector(factorial)).click();

        String res = chrome.findElement(new By.ById("sciOutPut")).getText();


        chrome.findElement(By.cssSelector(C)).click();
        Assertions.assertEquals("4.032914611 +26",res);
    }

}
