import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.jupiter.api.BeforeEach;

public class TestMail {

    public static ChromeDriver chrome;
    @BeforeEach
    public void onSetUp () {
        WebDriverManager.chromedriver().setup();
        chrome = new ChromeDriver();
        chrome.get("https://meta.ua/");
    }

    @Test
    public void test_Mail1_Send_Message() throws InterruptedException {

        // Precondition

        chrome.findElement(new By.ByXPath("/html/body/div[2]/section/div[1]/span/a[1]/label")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).sendKeys("IvanDon95");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).sendKeys("54987722224772");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/button")).click();
        Thread.sleep(3000);

        String a = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]/div[3]/div[1]/div/div/div[1]")).getText();
        int num = Integer.valueOf(a);

        //Steps

        chrome.findElement(new By.ById("id_send_email")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).sendKeys("ivandon95@meta.ua");
        Thread.sleep(500);
        chrome.findElement(new By.ById("composeareacontainer")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"body\"]")).sendKeys("Hello!");
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"b3_cnt\"]/div[2]/div[1]/div[1]/input[2]")).click();
        Thread.sleep(500);

        String actual1 = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]/div[3]/div[1]/div/div/div[1]/b")).getText();
        int act = num +Integer.valueOf(actual1);

        WebElement flag1 = chrome.findElement(new By.ByXPath("//*[@id=\"b3_c\"]"));
        Thread.sleep(500);
        flag1.findElement(new By.ByLinkText("Don Ivan")).click();
        Thread.sleep(500);

        String actual2 = chrome.findElement(new By.ByXPath("//*[@id=\"b3_c\"]/div[3]/div[2]")).getText();

        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete1\"]/div")).click();
        Thread.sleep(500);

        //Post condition

        chrome.findElement(new By.ByLinkText("Кошик")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"messlist\"]/tbody/tr[2]/td[1]/input")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete\"]")).click();
        Thread.sleep(500);

        // Result

        int expected1  = num + 1;
        String expected2 = "Hello!";
        Assertions.assertEquals(act,expected1);
        Assertions.assertEquals(actual2,expected2);
    }

    @Test
    public void test_Mail2_Delete_Message () throws InterruptedException {
// Precondition
        chrome.findElement(new By.ByXPath("/html/body/div[2]/section/div[1]/span/a[1]/label")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).sendKeys("IvanDon95");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).sendKeys("54987722224772");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/button")).click();
        Thread.sleep(3000);
// Steps
        chrome.findElement(new By.ById("id_send_email")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).sendKeys("ivandon95@meta.ua");
        Thread.sleep(500);
        chrome.findElement(new By.ById("composeareacontainer")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"body\"]")).sendKeys("Hello!");
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"b3_cnt\"]/div[2]/div[1]/div[1]/input[2]")).click();
        Thread.sleep(500);

        WebElement flag1 = chrome.findElement(new By.ByXPath("//*[@id=\"b3_c\"]"));
        Thread.sleep(500);
        flag1.findElement(new By.ByLinkText("Don Ivan")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete1\"]/div")).click();
        Thread.sleep(500);

        chrome.findElement(new By.ByLinkText("Кошик")).click();
        Thread.sleep(500);

        WebElement flag = chrome.findElement(new By.ByLinkText("Don Ivan"));
        flag.getText();
        // Result
        String actual = flag.getText();
        String expected = "Don Ivan";
        System.out.println(actual);
        Thread.sleep(500);
        Assertions.assertEquals(actual,expected);
// Post condition

        chrome.findElement(new By.ByXPath("//*[@id=\"messlist\"]/tbody/tr[2]/td[1]/input")).click();
        Thread.sleep(500);
        flag.findElement(new By.ByXPath("//*[@id=\"id_delete\"]")).click();
        Thread.sleep(500);

    }

    @Test
    public void test_Mail3_Incoming_Message () throws InterruptedException {
// Precondition

        chrome.findElement(new By.ByXPath("/html/body/div[2]/section/div[1]/span/a[1]/label")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).sendKeys("IvanDon95");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).sendKeys("54987722224772");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/button")).click();
        Thread.sleep(3000);

        String a = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]/div[3]/div[3]/div/div[1]")).getText();
        int num = Integer.valueOf(a);
        //Steps

        chrome.findElement(new By.ById("id_send_email")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).sendKeys("ivandon95@meta.ua");
        Thread.sleep(500);
        chrome.findElement(new By.ById("composeareacontainer")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"body\"]")).sendKeys("Hello!");
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"b3_cnt\"]/div[2]/div[1]/div[1]/input[2]")).click();
        Thread.sleep(500);

        String getNum = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]/div[3]/div[3]/div/div[1]")).getText();
        int act = Integer.valueOf(getNum);

        WebElement flag1 = chrome.findElement(new By.ByXPath("//*[@id=\"b3_c\"]"));
        Thread.sleep(500);
        flag1.findElement(new By.ByLinkText("Don Ivan")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete1\"]/div")).click();
        Thread.sleep(500);

        //Post condition

        WebElement flag2 = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]"));
        Thread.sleep(500);
        flag2.findElement(new By.ByLinkText("Надiсланi")).click();
        chrome.findElement(new By.ByXPath("//*[@id=\"allcheck\"]")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete\"]")).click();
        Thread.sleep(500);

        chrome.findElement(new By.ByLinkText("Кошик")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"allcheck\"]")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete\"]")).click();
        Thread.sleep(500);

        // Result
        int res = num + 1;
        int expected1  = res;
        Assertions.assertEquals(act,expected1);
    }

    @Test
    public void test_Mail4_ToSpam_Message () throws InterruptedException {
// Precondition

        chrome.findElement(new By.ByXPath("/html/body/div[2]/section/div[1]/span/a[1]/label")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[2]/input[1]")).sendKeys("IvanDon95");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/div[3]/input")).sendKeys("54987722224772");
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"login-form\"]/button")).click();
        Thread.sleep(3000);

        String a = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]/div[3]/div[4]/div/div[1]")).getText();
        int num = Integer.valueOf(a);
        //Steps

        chrome.findElement(new By.ById("id_send_email")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id='send_to']")).sendKeys("ivandon95@meta.ua");
        Thread.sleep(500);
        chrome.findElement(new By.ById("composeareacontainer")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"body\"]")).sendKeys("Hello!");
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"b3_cnt\"]/div[2]/div[1]/div[1]/input[2]")).click();
        Thread.sleep(500);

        WebElement flag1 = chrome.findElement(new By.ByXPath("//*[@id=\"b3_c\"]"));
        Thread.sleep(500);
        flag1.findElement(new By.ByLinkText("Don Ivan")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_spam1\"]/div")).click();
        Thread.sleep(500);

        String b = chrome.findElement(new By.ByXPath("//*[@id=\"left\"]/div[1]/div[3]/div[4]/div/div[1]")).getText();
        int act = Integer.valueOf(b);

        //Post condition

        chrome.findElement(new By.ByLinkText("Спам")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"messlist\"]/tbody/tr[2]/td[1]/input")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"id_delete\"]")).click();
        Thread.sleep(500);

        chrome.findElement(new By.ByXPath("//*[@id=\"id_settings\"]")).click();
        Thread.sleep(1000);
        chrome.findElement(new By.ByXPath("//*[@id=\"b3_cnt\"]/div[7]/a/strong")).click();
        Thread.sleep(500);
        chrome.findElement(new By.ByXPath("//*[@id=\"blacklist\"]")).clear();
        Thread.sleep(250);
        chrome.findElement(new By.ByXPath("//*[@id=\"b3_cnt\"]/form/div[4]/div[1]/div/input")).click();
        Thread.sleep(250);



        // Result
        int res = num + 1;
        int expected1  = res;
        Assertions.assertEquals(act,expected1);
    }

    @AfterEach
    public void close () {
        chrome.quit();
    }
}
