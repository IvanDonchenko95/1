package com.Task9_Product;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsProduct {
    ProductData productData = new ProductData(4545,"Borsh","8789878998","HouseWife",12.5,7);

    @Test
    public void test1_Set_Get_ID () {
        int actual = productData.setId(78);
        int expected = productData.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_Name () {
        String actual = productData.setName("Ivan");
        String expected = productData.getName();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_UPC () {
        String actual = productData.setuPC("Ivan");
        String expected = productData.getuPC();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_manufacturer() {
        String actual = productData.setManufacturer("By Me");
        String expected = productData.getManufacturer();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_Get_Price () {
        double actual = productData.setPrice(24.89);
        double expected = productData.getPrice();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_ShelfLife() {
        int actual = productData.setShelfLife(74);
        int expected = productData.getShelfLife();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test8_toString () {
        String actual = "ProductData{id=4545, name='Borsh', uPC='8789878998', manufacturer='HouseWife', price=12.5, shelfLife=7}";
        String expected = productData.toString();
        Assertions.assertEquals(actual,expected);
    }
}
