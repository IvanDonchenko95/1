package com.Task9_Product;

public class ProductData {

    private int id;
    private String name;
    private String uPC;
    private String manufacturer;
    private double price;
    private int shelfLife;

    public ProductData() {

    }

    @Override
    public String toString() {
        return "ProductData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", uPC='" + uPC + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                ", shelfLife=" + shelfLife +
                '}';
    }

    public ProductData(int id, String name, String uPC, String manufacturer, double price, int shelfLife) {
        this.id = id;
        this.name = name;
        this.uPC = uPC;
        this.manufacturer = manufacturer;
        this.price = price;
        this.shelfLife = shelfLife;
    }

    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String getuPC() {
        return uPC;
    }

    public String setuPC(String uPC) {
        this.uPC = uPC;
        return uPC;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
        return manufacturer;
    }

    public double getPrice() {
        return price;
    }

    public double setPrice(double price) {
        this.price = price;
        return price;
    }

    public int getShelfLife() {
        return shelfLife;
    }

    public int setShelfLife(int shelfLife) {
        this.shelfLife = shelfLife;
            return shelfLife;
    }

    public boolean start () {

        final int id = 522213;
        final String [] name = {"Hardwood Smoked Bacon","Original Precooked Bacon","Hickory Smoked Mild Pork Roll","Mild Premium Pork Sausage","Premium Deli Black Forest Ham","Premium Deli Smoked Honey Ham","All Purpose Eraser - Extra Strength","Disinfecting Wipes - Lemon Scent","Microfiber Glass & Window Cloth"};
        final String [] uPC= {"72527273070","72527255070","72588273070","72527273215","72527273870","72527278870","72527273070","72327273070","72527273070"};
        final String [] manufacturer = {"Halsa Foods","Califia Farms","Real Good Foods ","Halsa","JOYA","Crazy Richard's","Elmhurst 1925","Krave","Halsa"};
        double price = 7.5;
        int shelfLife = 5;


        double userPrice = 10;
        String findName = "Premium";
        int userShelfLife = 12;

        for (int i = 0; i < 9; i++) {
            ProductData productMain = new ProductData(id+i,name[i],uPC[i],manufacturer[i],price + (i + 5),shelfLife + (i +3));
            String a = name[i];
            if (a.contains(findName)) {
                System.out.println("By name = " + productMain.toString());
            }
        }

        for (int i = 0; i < 9; i++) {
            price = price + i;
            ProductData productMain = new ProductData(id+i,name[i],uPC[i],manufacturer[i],price+i,shelfLife + (i +3));
            String a = name[i];
            if (a.contains(findName)&&price > userPrice) {
                System.out.println("By price = " + productMain.toString());
            }
        }

        for (int i = 0; i < 9; i++) {
            shelfLife = shelfLife + (i+3);
            ProductData productMain = new ProductData(id+i,name[i],uPC[i],manufacturer[i],price + (i + 3),shelfLife);
            if (shelfLife > userShelfLife) {
                System.out.println("By shelfLife = " + productMain.toString());
            }
        }
        return true;
    }
}
