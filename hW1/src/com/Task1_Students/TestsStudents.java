package com.Task1_Students;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsStudents {

    StudentsData sd = new StudentsData("8956","Hryniuk","Alexander","Valeryevich","03.07.1996","Kharkiv,Lenina Ave, 74","+38(099)274-74-56","IT Technology","2","AT/QA");

    @Test
    public void test1_Set_Get_ID () {
        String actual = sd.setId("5");
        String expected = sd.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_Surname () {
        String actual = sd.setSurname("Hryniuk");
        String expected = sd.getSurname();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Name () {
        String actual = sd.setName("Ivan");
        String expected = sd.getName();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_Patronymic () {
        String actual = sd.setPatronymic("Vladimirovich");
        String expected = sd.getPatronymic();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test5_Set_Get_BirthDay () {
        String actual = sd.setBirthday("03.08.1996");
        String expected = sd.getBirthday();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_Address () {
        String actual = sd.setAddress("Kharkiv");
        String expected = sd.getAddress();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_PhoneNumber () {
        String actual = sd.setPhone("+38(099)-225-65-89");
        String expected = sd.getPhone();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test8_Set_Get_Faculty () {
        String actual = sd.setFaculty("QA Technology");
        String expected = sd.getFaculty();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test9_Set_Get_Course () {
        String actual = sd.setCourse("2");
        String expected = sd.getCourse();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test10_Set_Get_Group () {
        int actual = sd.setGroup("2");
        String expected = sd.getGroup();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test11_toString () {
        String actual = "StudentsData {  id = 8956 , surname = Hryniuk, name = Alexander , patronymic = Valeryevich,birthday = 03.07.1996 ,address = Kharkiv,Lenina Ave, 74 , phone = +38(099)274-74-56, faculty = IT Technology, course = 2, group = AT/QA }";
        String expected = sd.toString();
        Assertions.assertEquals(actual,expected);
    }

}
