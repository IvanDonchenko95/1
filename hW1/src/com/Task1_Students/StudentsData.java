package com.Task1_Students;

public class StudentsData {

    private String id;
    private String surname;
    private String name;
    private String patronymic;
    private String birthday;
    private String address;
    private String phone;
    private String faculty;
    private String course;
    private String group;


    public StudentsData (String id,String surname,String name,String patronymic,String birthday,String address,String phone,String faculty ){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday ;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
    }

    public StudentsData (String id,String surname,String name,String patronymic,String birthday,String address,String phone,String faculty,String course ){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday ;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
    }

    public StudentsData (String id,String surname,String name,String patronymic,String birthday,String address,String phone,String faculty,String course,String group ){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday ;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public StudentsData (String id,String surname,String name,String patronymic,String birthday,String address,String phone ){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday ;
        this.address = address;
        this.phone = phone;
    }

    public StudentsData() {

    }

    public String getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getCourse() {
        return course;
    }

    public String getGroup() {
        return group;
    }

    public String setId(String id) {
        this.id = id;
        return id;
    }

    public String setSurname(String surname) {
        this.surname = surname;
        return surname;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return patronymic;
    }

    public String setBirthday(String birthday) {
        this.birthday = birthday;
        return birthday;
    }

    public String setAddress(String address) {
        this.address = address;
        return address;
    }

    public String setPhone(String phone) {
        this.phone = phone;
        return phone;
    }

    public String setFaculty(String faculty) {
        this.faculty = faculty;
        return faculty;
    }

    public String setCourse(String course) {
        this.course = course;
        return course;
    }

    public int setGroup(String group) {
        this.group = group;
        return 0;
    }

    @Override
    public String toString() {
        return "StudentsData { " + " id = " + id + " , surname = " + surname + ", name = " + name + " , patronymic = " + patronymic + ",birthday = " + birthday + " ,address = " + address + " , phone = " + phone + ", faculty = " + faculty + ", course = " + course + ", group = " + group  + " }";
    }


    public boolean start() {

        final String [] surname = {"Hryniuk","Hryhoruk","Mykhailuk","Yurskich","Tsar","Melnik","Shevchuk","Tkachenko","Moroz","Lysenko","Kolecnik","Juk"};
        final String [] name = {"Alexander","Anastasia","Daniel","Svyatoslav","Denis","German","Alice","Angelina","Lisa","Sophia","Maria","Natalie"};
        final String [] patronymic = {"Valeryevich","Valeryevna","Victorovich","Mikhaylovich","Vasilyevich","Victorovich","Nikolayevna","Mikhaylovna","Vladimirovna","Filipovna","Ivanovna","Fyodorovna"};
        final String [] birthday = {"03.07.1996","05.08.1997","01.01.1995","02.03.1997","12.06.1995","28.09.1997","26.06.1996","15.07.1995","09.09.1997","25.02.1998","18.10.1997","12.12.1996"};
        final String [] address = {"Kharkiv,Lenina Ave, 74","Kharkiv,Nauky Ave, 68","Kharkiv,Lenina Ave, 66А","Kharkiv,Tobol's'ka St, 46Б","Kharkiv,Otakara Yarosha St, 21Б","Kharkiv,Varenykivs'kyi Ln, 14","Kharkiv,Dmytra Vyshnevetskoho St, 56","Kharkiv,Barykadnyi Ln, 2","Kharkiv,Alushtyns'ka St, 3","Kharkiv,Novocherkas'kyi Ln, 12","Kharkiv,Peremozhtsiv Ln, 14","Kharkiv,Poltavskyi Shliakh St, 188-10","Poltavskyi Shliakh St, 144"};
        final String [] phone = {"+38(099)274-74-56", "+38(099)625-67-75", "+38(099)235-53-60", "+38(099)537-91-63", "+38(099)380-83-79","+38(073)438-40-20","+38(073)032-66-28", "+38(073)795-32-51", "+38(098)460-68-29", "+38(098)933-46-67", "+38(098)417-78-86", "+38(098)977-87-73"};
        final String faculty = "IT Technology";

        for (int i = 0; i < 7; i++) {

            StudentsData sd = new StudentsData("658",surname[i],name[i],patronymic[i],"1"+i +"." + "0"+ (12 - i) + "." + "1997",address[i],phone[i],faculty);
            System.out.println("Faculty = " + sd.toString());
        }


        for (int i = 0; i < 7; i++) {

            StudentsData sd = new StudentsData("8956",surname[i],name[i],patronymic[i],birthday[i],address[i],phone[i],faculty,"1");
            System.out.println("Faculty, Course = " + sd.toString());
        }

        for (int i = 0; i < 7; i++) {

            StudentsData sd = new StudentsData("8994",surname[i],name[i],patronymic[i],"1"+i +"." + "0"+ (12 - i) + "." + "1997",address[i],phone[i],faculty);
            System.out.println("By year = " + sd.toString());
        }

        for (int i = 0; i < 1; i++) {

            StudentsData sd = new StudentsData("8956","Hryniuk","Alexander","Valeryevich","03.07.1996","Kharkiv,Lenina Ave, 74","+38(099)274-74-56","IT Technology","2","AT/QA");
            System.out.println("Group = " + sd.toString());
        }

        return true;
    }
}
