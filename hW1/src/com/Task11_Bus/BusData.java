package com.Task11_Bus;

public class BusData {

    private String surname;
    private int busNum;
    private String numOfdirection;
    private String carBrand;
    private int yearOfOperation;
    private int mileage;

    public BusData() {

    }

    @Override
    public String toString() {
        return "BusData{" +
                "surname='" + surname + '\'' +
                ", busNum=" + busNum +
                ", numOfdirection='" + numOfdirection + '\'' +
                ", carBrand='" + carBrand + '\'' +
                ", yearOfOperation='" + yearOfOperation + '\'' +
                ", mileage=" + mileage +
                '}';
    }

    public BusData(String surname, int busNum, String numOfdirection, String carBrand, int yearOfOperation, int mileage) {
        this.surname = surname;
        this.busNum = busNum;
        this.numOfdirection = numOfdirection;
        this.carBrand = carBrand;
        this.yearOfOperation = yearOfOperation;
        this.mileage = mileage;
    }

    public String getSurname() {
        return surname;
    }

    public String setSurname(String surname) {
        this.surname = surname;
        return surname;
    }

    public int getBusNum() {
        return busNum;
    }

    public int setBusNum(int busNum) {
        this.busNum = busNum;
        return busNum;
    }

    public String getNumOfdirection() {
        return numOfdirection;
    }

    public String setNumOfdirection(String numOfdirection) {
        this.numOfdirection = numOfdirection;
        return numOfdirection;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String setCarBrand(String carBrand) {
        this.carBrand = carBrand;
        return carBrand;
    }

    public int getYearOfOperation() {
        return yearOfOperation;
    }

    public int setYearOfOperation(int yearOfOperation) {
        this.yearOfOperation = yearOfOperation;
        return yearOfOperation;
    }

    public int getMileage() {
        return mileage;
    }

    public int setMileage(int mileage) {
        this.mileage = mileage;
        return mileage;
    }

    public boolean start () {


        final String [] surname = {"Hryniuk A.S","Hryhoruk V.M","Mykhailuk E.R","Yurskich R.E","Tsar A.V","Melnik S.D","Shevchuk F.F"};
        int busNum = 1;
        final String [] numOfdirection = {"A-984-CY","D-995-DF","T-878-RE","D-888-UY","A-984-CY","S-878-LK","H-897-MN"};
        final String[] carBrand = {"BMW","Honda","Volkswagen","Ford","Hyundai","Audi","Tesla"};
        int yearOfOperation = 8;
        int mileage = 40000;

        String userNumOfdirection = "A-984-CY";

        for (int i = 0; i < 7; i++) {
            BusData busData = new BusData(surname[i],busNum + i,numOfdirection[i],carBrand[i],yearOfOperation + i,mileage + (i * 1000));
            if (numOfdirection[i].contains(userNumOfdirection)) {
                System.out.println("By direction = " + busData.toString());
            }
        }
        for (int i = 0; i < 7; i++) {
            yearOfOperation = yearOfOperation + i;
            BusData busData = new BusData(surname[i],busNum + i,numOfdirection[i],carBrand[i],yearOfOperation + i,mileage + (i * 1000));
            if (yearOfOperation > 10 ) {
                System.out.println("By year Of operation = " + busData.toString());
            }
        }

        for (int i = 0; i < 7; i++) {
            mileage = mileage + (i * 10000);
            BusData busData = new BusData(surname[i],busNum + i,numOfdirection[i],carBrand[i],yearOfOperation + i,mileage);
            if (mileage > 100000 ) {
                System.out.println("By mileage = " + busData.toString());
            }
        }


        return true;
    }
}
