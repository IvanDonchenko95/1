package com.Task11_Bus;

import org.junit.jupiter.api.*;

public class TestsBus {

BusData busData = new BusData("Tsar A.V",5,"A-984-CY","Hyundai",33,140000);



    @Test
    public void test1_Set_Get_Surname () {
        String actual = busData.setSurname("Bora Bora");
        String expected = busData.getSurname();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_BusNum () {
        int actual = busData.setBusNum(35);
        int expected = busData.getBusNum();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_numOfdirectione () {
        String actual = busData.setNumOfdirection("12");
        String expected = busData.getNumOfdirection();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_CarBrand() {
        String actual = busData.setCarBrand("12");
        String expected = busData.getCarBrand();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_YearOfOperation() {
        int actual = busData.setYearOfOperation(120);
        int expected = busData.getYearOfOperation();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Mileage() {
        int actual = busData.setMileage(120);
        int expected = busData.getMileage();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_toString () {
    String actual = "BusData{surname='Tsar A.V', busNum=5, numOfdirection='A-984-CY', carBrand='Hyundai', yearOfOperation='33', mileage=140000}";
    String expected = busData.toString();
    Assertions.assertEquals(actual,expected);
    }

}
