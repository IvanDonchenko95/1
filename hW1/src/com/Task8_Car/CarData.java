package com.Task8_Car;

public class CarData {

    private int Id;
    private String carBrand;
    private String carModel;
    private int release;
    private String color;
    private int price;
    private long numOfReg;

    public CarData(int id, String carBrand, String carModel, int release, String color, int price, long numOfReg) {
        Id = id;
        this.carBrand = carBrand;
        this.carModel = carModel;
        this.release = release;
        this.color = color;
        this.price = price;
        this.numOfReg = numOfReg;
    }

    public CarData() {

    }

    @Override
    public String toString() {
        return "CarData{" +
                "Id=" + Id +
                ", carBrand='" + carBrand + '\'' +
                ", carModel='" + carModel + '\'' +
                ", release='" + release + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", numOfReg=" + numOfReg +
                '}';
    }

    public int getId() {
        return Id;
    }

    public int setId(int id) {
        Id = id;
        return id;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String setCarBrand(String carBrand) {
        this.carBrand = carBrand;
        return carBrand;
    }

    public String getCarModel() {
        return carModel;
    }

    public String setCarModel(String carModel) {
        this.carModel = carModel;
        return carModel;
    }

    public int getRelease() {
        return release;
    }

    public int setRelease(int release) {
        this.release = release;
        return release;
    }

    public String getColor() {
        return color;
    }

    public String setColor(String color) {
        this.color = color;
        return color;
    }

    public int getPrice() {
        return price;
    }

    public int setPrice(int price) {
        this.price = price;
        return price;
    }

    public long getNumOfReg() {
        return numOfReg;
    }

    public long setNumOfReg(long numOfReg) {
        this.numOfReg = numOfReg;
        return numOfReg;
    }

    public boolean start () {


        final int id = 5557878;
        final String[] carBrand = {"BMW","Honda","Volkswagen","Ford","Hyundai","Audi","Tesla"};
        final String [] carModel = {"X5","Civic","Golf","F-150","Tuscon","Q8","Model - S"};
        final int release = 2014;
        final String[] color = {"Red","Blue","White","Grey","White","Black","Green","Yellow"};
        final int [] price = {35000,25000,5000,5300,7000,25000,40000} ;
        final long numOfReg = 4_65_534_13;

        String findCar = "Ford";
        String findModel = "Civic";
        int time = 2020;
        int exploatation = 4;
        int yearfoRelese = 2010;
        int findPrice = 10000;

        for (int i = 0; i < 6; i++) {
            CarData cars = new CarData(id+i,carBrand[i],carModel[i],release + i,color[i],price[i],numOfReg+i);
            if (carBrand[i].contentEquals(findCar)){
                System.out.println("Car Brand = " + cars.toString());
            }
        }

        for (int i = 0; i < 6; i++) {
            CarData cars = new CarData(id+i,carBrand[i],carModel[i],release + i,color[i],price[i],numOfReg+i);
            if (carModel[i].contentEquals(findModel) && time - release > exploatation) {
                System.out.println("Car model = " + cars.toString());
            }
        }

        for (int i = 0; i < 6; i++) {
            CarData cars = new CarData(id+i,carBrand[i],carModel[i],release + i,color[i],price[i],numOfReg+i);
            if (release > yearfoRelese && price[i] > findPrice) {
                System.out.println("Car price = " + cars.toString());
            }
        }

        return true;
    }
}
