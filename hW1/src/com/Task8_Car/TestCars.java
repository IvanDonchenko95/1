package com.Task8_Car;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCars {

    CarData carData =new CarData(5557881,"Ford","F-150",2017,"Grey",5300,46553416);

    @Test
    public void test1_Set_Get_ID () {
        int actual = carData.setId(78);
        int expected = carData.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_carBrand () {
        String actual = carData.setCarBrand("Ivan");
        String expected = carData.getCarBrand();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_carModel () {
        String actual = carData.setCarModel("Ivan");
        String expected = carData.getCarModel();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_Release() {
        int actual = carData.setRelease(2015);
        int expected = carData.getRelease();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_Get_Color () {
        String actual = carData.setColor("White");
        String expected = carData.getColor();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_Price() {
        int actual = carData.setPrice( 74);
        int expected = carData.getPrice();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_numOfReg() {
        long actual = carData.setNumOfReg(87564);
        long expected = carData.getNumOfReg();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test8_toString () {
        String actual = "CarData{Id=5557881, carBrand='Ford', carModel='F-150', release='2017', color='Grey', price=5300, numOfReg=46553416}";
        String expected = carData.toString();
        Assertions.assertEquals(actual,expected);
    }


}
