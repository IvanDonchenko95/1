package com.Task2_Customer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsCustomer {

    CustomerData customer2 = new CustomerData(3587,"Hryniuk","Alexander","Valeryevich","Kharkiv,Lenina Ave, 74","4565 9877 4566 3211","UA2545674565210000056975");


    @Test
    public void test1_Set_Get_ID () {
        int actual = customer2.setId(5);
        int expected = customer2.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_Surname () {
        String actual = customer2.setSurname("Hryniuk");
        String expected = customer2.getSurname();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Name () {
        String actual = customer2.setName("Ivan");
        String expected = customer2.getName();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_Patronymic () {
        String actual = customer2.setPatronymic("Vladimirovich");
        String expected = customer2.getPatronymic();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test5_Set_Get_CardNum () {
        String actual = customer2.setCardNum("03.08.1996");
        String expected = customer2.getCardNum();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_Address () {
        String actual = customer2.setAddress("Kharkiv");
        String expected = customer2.getAddress();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_BankAccount () {
        String actual = customer2.setBankAccount("+38(099)-225-65-89");
        String expected = customer2.getBankAccount();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test10_toString () {
        String actual = "CustomerData{id=3587, surname='Hryniuk', name='Alexander', patronymic='Valeryevich', address='Kharkiv,Lenina Ave, 74', cardNum='4565 9877 4566 3211', bankAccount='UA2545674565210000056975'}";
        String expected = customer2.toString();
        Assertions.assertEquals(actual,expected);
    }

}
