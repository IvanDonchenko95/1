package com.Task2_Customer;

public class CustomerData {

    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private String cardNum;
    private String bankAccount;

    public CustomerData() {
    }


    @Override
    public String toString() {
        return "CustomerData{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                ", cardNum='" + cardNum + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                '}';
    }


    public CustomerData(int id, String surname, String name, String patronymic, String address) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
    }

    public CustomerData(int id, String surname, String name, String patronymic, String address, String cardNum, String bankAccount) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.cardNum = cardNum;
        this.bankAccount = bankAccount;

    }


    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String setSurname(String surname) {
        this.surname = surname;
        return surname;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public String setAddress(String address) {
        this.address = address;
        return address;
    }

    public String getCardNum() {
        return cardNum;
    }

    public String setCardNum(String cardNum) {
        this.cardNum = cardNum;
        return cardNum;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public String setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
        return bankAccount;
    }

    public boolean start () {

        final int [] id = {3587};
        final String [] surname = {"Hryniuk","Hryhoruk","Mykhailuk","Yurskich","Tsar","Melnik","Shevchuk","Tkachenko","Moroz","Lysenko","Kolecnik","Juk"};
        final String [] name = {"Alexander","Anastasia","Daniel","Svyatoslav","Denis","German","Alice","Angelina","Lisa","Sophia","Maria","Natalie"};
        final String [] patronymic = {"Valeryevich","Valeryevna","Victorovich","Mikhaylovich","Vasilyevich","Victorovich","Nikolayevna","Mikhaylovna","Vladimirovna","Filipovna","Ivanovna","Fyodorovna"};
        final String [] address = {"Kharkiv,Lenina Ave, 74","Kharkiv,Nauky Ave, 68","Kharkiv,Lenina Ave, 66А","Kharkiv,Tobol's'ka St, 46Б","Kharkiv,Otakara Yarosha St, 21Б","Kharkiv,Varenykivs'kyi Ln, 14","Kharkiv,Dmytra Vyshnevetskoho St, 56","Kharkiv,Barykadnyi Ln, 2","Kharkiv,Alushtyns'ka St, 3","Kharkiv,Novocherkas'kyi Ln, 12","Kharkiv,Peremozhtsiv Ln, 14","Kharkiv,Poltavskyi Shliakh St, 188-10","Poltavskyi Shliakh St, 144"};
        final String [] bankAccount = {"UA2545674565210000056975"};
        final  String [] cardNum = {"4565 8778 7898 9877","4565 4546 5477 7787","5456 6544 4777 1122","5654 3211 1244 5666","5456 5454 9877 8788","3123 2122 3558 7488","7898 8755 4565 6565","8978 8756 4555 5444","7899 9877 5564 4545","4545 6666 2122 2222","4545 6465 9887 7788","5454 6666 5555 8888"};
        for (int i = 0; i < 7; i++) {

            CustomerData cD = new CustomerData(id[0]+i,surname[i],name[i],patronymic[i],address[i]);
            System.out.println("By name = " + cD.toString());
        }

        for (int i = 0; i < 1; i++) {

            CustomerData cD = new CustomerData(id[i],surname[i],name[i],patronymic[i],address[i],cardNum[i],bankAccount[i]);
            System.out.println("By card = " + cD.toString());
        }
        return true;
    }
}
