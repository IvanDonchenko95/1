package com.Task5_Books;

import com.Task10_Train.TrainData;

public class BooksData {

    private int id;
    private String bookNAme;
    private String authorName;
    private String publishingHouse;
    private String yearOfPubl;
    private int numOfPages;
    private double price;
    private String binding;

    public BooksData() {
    }

    @Override
    public String toString() {
        return "BooksData{" +
                "id=" + id +
                ", bookNAme='" + bookNAme + '\'' +
                ", authorName='" + authorName + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                ", yearOfPubl='" + yearOfPubl + '\'' +
                ", numOfPages=" + numOfPages +
                ", price=" + price +
                ", binding='" + binding + '\'' +
                '}';
    }

    public BooksData(int id, String bookNAme, String authorName, String publishingHouse, String yearOfPubl, int numOfPages, double price, String binding) {
        this.id = id;
        this.bookNAme = bookNAme;
        this.authorName = authorName;
        this.publishingHouse = publishingHouse;
        this.yearOfPubl = yearOfPubl;
        this.numOfPages = numOfPages;
        this.price = price;
        this.binding = binding;
    }

    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public String getBookNAme() {
        return bookNAme;
    }

    public String setBookNAme(String bookNAme) {
        this.bookNAme = bookNAme;
        return bookNAme;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String setAuthorName(String authorName) {
        this.authorName = authorName;
        return authorName;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public String setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
        return publishingHouse;
    }

    public String getYearOfPubl() {
        return yearOfPubl;
    }

    public String setYearOfPubl(String yearOfPubl) {
        this.yearOfPubl = yearOfPubl;
        return yearOfPubl;
    }

    public int getNumOfPages() {
        return numOfPages;
    }

    public int setNumOfPages(int numOfPages) {
        this.numOfPages = numOfPages;
        return numOfPages;
    }

    public double getPrice() {
        return price;
    }

    public double setPrice(double price) {
        this.price = price;
        return price;
    }

    public String getBinding() {
        return binding;
    }

    public String setBinding(String binding) {
        this.binding = binding;
        return binding;
    }

    public boolean start () {

        final int id = 126585;
        final String [] bookName = {"The Great Gatsby","One Hundred Years of Solitude","A Passage to India","Invisible Man","Don Quixote","Beloved"," Hamlet"};
        final String [] authorName = {"F. Scott Fitzgerald"," Gabriel García Márquez","E.M. Forster","Ralph Ellison","Miguel de Cervantes","Toni Morrison","William Shakespeare"};
        final String [] publishingHouse = {"Gannett Company","HarperCollins","Ablex Publishing","Addison-Wesley","Tokyopop","Simon & Schuster","Stellar Publishing Corporation"};
        final String [] yearOfPubl = {"1992","2004","1999","2015","2001","2012","2018"};
        final String [] binding = {"Hard cover","Soft cover"};
        final int numOfPages = 250;
        final double price = 12.99;

        for (int i = 0; i < 6; i++) {
            BooksData booksO = new BooksData (id + i,bookName[i],authorName[2],publishingHouse[i],yearOfPubl[i],numOfPages + (i * 9),price + 0.95,binding[0]);
            if (i % 2 == 0) {
                booksO.setBinding(binding[0]);
            }
            if (i % 2 != 0) {
                booksO.setBinding(binding[1]);
            }
            System.out.println("One author = " + booksO.toString());

        }

        for (int i = 0; i < 6; i++) {
            BooksData booksO = new BooksData (id + i,bookName[i],authorName[i],publishingHouse[1],yearOfPubl[i],numOfPages + (i * 9),price + 0.95,binding[0]);
            if (i % 2 == 0) {
                booksO.setBinding(binding[0]);
            }
            if (i % 2 != 0) {
                booksO.setBinding(binding[1]);
            }
            System.out.println("Publish house = " + booksO.toString());
        }

        for (int i = 0; i < 6; i++) {
            BooksData booksO = new BooksData (id + i,bookName[i],authorName[i],publishingHouse[i],yearOfPubl[2],numOfPages + (i * 9),price + 0.95,binding[0]);
            if (i % 2 == 0) {
                booksO.setBinding(binding[0]);
            }
            if (i % 2 != 0) {
                booksO.setBinding(binding[1]);
            }
            System.out.println("By year = " + booksO.toString());
        }

        return true;
    }

}
