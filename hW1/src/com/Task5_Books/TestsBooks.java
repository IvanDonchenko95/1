package com.Task5_Books;


import org.junit.jupiter.api.*;

public class TestsBooks {

    BooksData bD = new BooksData(126585,"The Great Gatsby","F. Scott Fitzgerald","Gannett Company","1999",250,13.94,"Hard cover");

    @Test
    public void test1_Set_Get_ID () {
        int actual = bD.setId(78);
        int expected = bD.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_BooksName () {
        String actual = bD.setBookNAme("Hryniuk");
        String expected = bD.getBookNAme();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_AuthorName () {
        String actual = bD.setAuthorName("Ivan");
        String expected = bD.getAuthorName();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_publishingHouse() {
        String actual = bD.setPublishingHouse("Vladimirovich");
        String expected = bD.getPublishingHouse();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_Get_yearOfPubl () {
        String actual = bD.setYearOfPubl("203");
        String expected = bD.getYearOfPubl();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_NumOfPages () {
        int actual = bD.setNumOfPages(258);
        int expected = bD.getNumOfPages();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_Price() {
        double actual = bD.setPrice(65.1);
        double expected = bD.getPrice();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test8_Set_Get_Bining() {
        String actual = bD.setBinding("Soft");
        String expected = bD.getBinding();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test9_toString () {
        String actual = "BooksData{id=126585, bookNAme='The Great Gatsby', authorName='F. Scott Fitzgerald', publishingHouse='Gannett Company', yearOfPubl='1999', numOfPages=250, price=13.94, binding='Hard cover'}";
        String expected = bD.toString();
        Assertions.assertEquals(actual,expected);
    }
        }
