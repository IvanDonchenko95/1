package com.Task4_Abiturient;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsAbiturient {

        AbiturientData abiturientData = new AbiturientData(3588,"Moroz","Lisa","Vladimirovna","Kharkiv,Alushtyns'ka St, 3","+38(098)460-68-29",98);

        @Test
        public void test1_Set_Get_ID () {
            int actual = abiturientData.setId(78);
            int expected = abiturientData.getId();
            Assertions.assertEquals(actual,expected);
        }

        @Test
        public void test2_Set_Get_Surname () {
            String actual = abiturientData.setSurname("Hryniuk");
            String expected = abiturientData.getSurname();
            Assertions.assertEquals(actual,expected);
        }

        @Test
        public void test3_Set_Get_Name () {
            String actual = abiturientData.setName("Ivan");
            String expected = abiturientData.getName();
            Assertions.assertEquals(actual,expected);
        }

        @Test
        public void test4_Set_Get_Patronymic () {
            String actual = abiturientData.setPatronymic("Vladimirovich");
            String expected = abiturientData.getPatronymic();
            Assertions.assertEquals(actual,expected);
        }


        @Test
        public void test5_Set_Get_Address () {
            String actual = abiturientData.setAddress("Kharkiv");
            String expected = abiturientData.getAddress();
            Assertions.assertEquals(actual,expected);
        }

        @Test
        public void test6_Set_Get_PhoneNumber () {
            String actual = abiturientData.setPhone("+38(099)-225-65-89");
            String expected = abiturientData.getPhone();
            Assertions.assertEquals(actual,expected);
        }

        @Test
        public void test7_Set_Get_Mark () {
            int actual = abiturientData.setMark(65);
            int expected = abiturientData.getMark();
            Assertions.assertEquals(actual,expected);
        }

        @Test
        public void test9_toString () {
            String actual = "AbiturientData{id=3588, surname='Moroz', name='Lisa', patronymic='Vladimirovna', address='Kharkiv,Alushtyns'ka St, 3', phone='+38(098)460-68-29', mark=98}";
            String expected = abiturientData.toString();
            Assertions.assertEquals(actual,expected);
        }

}
