package com;

import com.Task10_Train.TrainData;
import com.Task11_Bus.BusData;
import com.Task12_Airlines.AirLinesData;
import com.Task1_Students.StudentsData;
import com.Task2_Customer.CustomerData;
import com.Task3_Patient.PatientData;
import com.Task4_Abiturient.AbiturientData;
import com.Task5_Books.BooksData;
import com.Task6_House.HouseData;
import com.Task7_Phone.PhoneData;
import com.Task8_Car.CarData;
import com.Task9_Product.ProductData;


public class Main {

    public static void main(String[] args) {

        StudentsData studentsData = new StudentsData();
        System.out.println(studentsData.start() + " Task_1 ");

        CustomerData customerData = new CustomerData();
        System.out.println(customerData.start() + " Task_2 " );

        PatientData patientData = new PatientData();
        System.out.println( patientData.start() + " Task_3 ");

        AbiturientData abiturientData= new AbiturientData();
        System.out.println(abiturientData.start() + " Task_4");

        BooksData booksData= new BooksData();
        System.out.println(booksData.start() + " Task_5");

        HouseData houseData = new HouseData();
        System.out.println(houseData.start() + " Task_6");

        PhoneData phoneData = new PhoneData();
        System.out.println(phoneData.start() + " Task_7");

        CarData carData = new CarData();
        System.out.println(carData.start() + " Task_8");

        ProductData productData = new ProductData();
        System.out.println(productData.start() + " Task_9");

        TrainData trainData = new TrainData();
        System.out.println(trainData.start()+ " Task_10");

        BusData busData = new BusData();
        System.out.println(busData.start() + " Task_11");

        AirLinesData airLinesData = new AirLinesData();
        System.out.println(airLinesData.start()+ " Task_12");


    }

}
