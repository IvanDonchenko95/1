package com.Task7_Phone;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsPhone {

PhoneData phoneData = new PhoneData(3592,"Melnik","German","Victorovich","Kharkiv,Varenykivs'kyi Ln, 14","3123 2122 3558 7488",999840,1952,118324);

    @Test
    public void test1_Set_Get_ID () {
        int actual = phoneData.setId(78);
        int expected = phoneData.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_Surname () {
        String actual = phoneData.setSurname("Ivan");
        String expected = phoneData.getSurname();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Name () {
        String actual = phoneData.setName("Ivan");
        String expected = phoneData.getName();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_Patronymic() {
        String actual = phoneData.setPatronymic("phoneData");
        String expected = phoneData.getPatronymic();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_Get_Address () {
        String actual = phoneData.setAddress("Vsds");
        String expected = phoneData.getAddress();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_CardNum() {
        String actual = phoneData.setCardNum("Kharkiv,Lenina Ave, 74");
        String expected = phoneData.getCardNum();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_Debit() {
        int actual = phoneData.setDebit(87564);
        int expected = phoneData.getDebit();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test8_Set_Get_Credit() {
        int actual = phoneData.setCredit(54646554);
        int expected = phoneData.getCredit();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test9_Set_Get_Credit() {
        long actual = phoneData.setTimeTolk(54646554);
        long expected = phoneData.getTimeTolk();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test10_toString () {
        String actual = "PhoneData{id=3592, surname='Melnik', name='German', patronymic='Victorovich', address='Kharkiv,Varenykivs'kyi Ln, 14', cardNum='3123 2122 3558 7488', debit=999840, credit=1952, timeTolk=118324}";
        String expected = phoneData.toString();
        Assertions.assertEquals(actual,expected);
    }

}
