package com.Task7_Phone;

public class PhoneData {

    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private String cardNum;
    private int debit;
    private int credit;
    private long timeTolk;

    public PhoneData() {

    }

    @Override
    public String toString() {
        return "PhoneData{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                ", cardNum='" + cardNum + '\'' +
                ", debit=" + debit +
                ", credit=" + credit +
                ", timeTolk=" + timeTolk +
                '}';
    }

    public PhoneData(int id, String surname, String name, String patronymic, String address, String cardNum, int debit, int credit, long timeTolk) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.cardNum = cardNum;
        this.debit = debit;
        this.credit = credit;
        this.timeTolk = timeTolk;
    }

    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String setSurname(String surname) {
        this.surname = surname;
        return surname;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public String setAddress(String address) {
        this.address = address;
        return address;
    }

    public String getCardNum() {
        return cardNum;
    }

    public String setCardNum(String cardNum) {
        this.cardNum = cardNum;
        return cardNum;
    }

    public int getDebit() {
        return debit;
    }

    public int setDebit(int debit) {
        this.debit = debit;
        return debit;
    }

    public int getCredit() {
        return credit;
    }

    public int setCredit(int credit) {
        this.credit = credit;
        return credit;
    }

    public long getTimeTolk() {
        return timeTolk;
    }

    public long setTimeTolk(long timeTolk) {
        this.timeTolk = timeTolk;
        return timeTolk;
    }

    public boolean start () {

        final int  id = 3587;
        final String [] surname = {"Hryniuk","Hryhoruk","Mykhailuk","Yurskich","Tsar","Melnik","Shevchuk","Tkachenko","Moroz","Lysenko","Kolecnik","Juk"};
        final String [] name = {"Alexander","Anastasia","Daniel","Svyatoslav","Denis","German","Alice","Angelina","Lisa","Sophia","Maria","Natalie"};
        final String [] patronymic = {"Valeryevich","Valeryevna","Victorovich","Mikhaylovich","Vasilyevich","Victorovich","Nikolayevna","Mikhaylovna","Vladimirovna","Filipovna","Ivanovna","Fyodorovna"};
        final String [] address = {"Kharkiv,Lenina Ave, 74","Kharkiv,Nauky Ave, 68","Kharkiv,Lenina Ave, 66А","Kharkiv,Tobol's'ka St, 46Б","Kharkiv,Otakara Yarosha St, 21Б","Kharkiv,Varenykivs'kyi Ln, 14","Kharkiv,Dmytra Vyshnevetskoho St, 56","Kharkiv,Barykadnyi Ln, 2","Kharkiv,Alushtyns'ka St, 3","Kharkiv,Novocherkas'kyi Ln, 12","Kharkiv,Peremozhtsiv Ln, 14","Kharkiv,Poltavskyi Shliakh St, 188-10","Poltavskyi Shliakh St, 144"};
        final String [] cardNum = {"4565 8778 7898 9877","4565 4546 5477 7787","5456 6544 4777 1122","5654 3211 1244 5666","5456 5454 9877 8788","3123 2122 3558 7488","7898 8755 4565 6565","8978 8756 4555 5444","7899 9877 5564 4545","4545 6666 2122 2222","4545 6465 9887 7788","5454 6666 5555 8888"};
        final int debit = 1000000;
        final int credit = 1477;
         long timeTalk = 1244;

        for (int i = 0; i < 8; i++) {
            timeTalk = timeTolk + (i * 850);
            PhoneData longTolk = new PhoneData(id + i,surname[i],name[i],patronymic[i],address[i],cardNum[i],debit - (i*32),credit + (i*95),timeTolk);
            if (timeTolk < 2500){
                System.out.println("Long talk = " + longTolk.toString());
            }
        }

        for (int i = 0; i < 8; i++) {
            timeTolk = timeTolk + (i*5120);
            char a = 'M';
            PhoneData alphabet = new PhoneData(id + i,surname[i],name[i],patronymic[i],address[i],cardNum[i],debit - (i*32),credit + (i*95),timeTolk + (i * 8056));
            if (surname[i].charAt(0) == a){
                System.out.println("By alphabet = " + alphabet.toString());
            }
        }

        return true;
    }

}
