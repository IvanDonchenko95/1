package com.Task12_Airlines;

public class AirLinesData {

    private String destination;
    private String flightumber;
    private String typeOfPlane;
    private long dispatchime;
    private String dayOfWeek;

    public AirLinesData() {

    }

    @Override
    public String toString() {
        return "AirLinesData{" +
                "destination='" + destination + '\'' +
                ", flightumber='" + flightumber + '\'' +
                ", typeOfPlane='" + typeOfPlane + '\'' +
                ", dispatchime=" + dispatchime +
                ", dayOfWeek='" + dayOfWeek + '\'' +
                '}';
    }

    public AirLinesData(String destination, String flightumber, String typeOfPlane, long dispatchime, String dayOfWeek) {
        this.destination = destination;
        this.flightumber = flightumber;
        this.typeOfPlane = typeOfPlane;
        this.dispatchime = dispatchime;
        this.dayOfWeek = dayOfWeek;
    }

    public String getDestination() {
        return destination;
    }

    public String setDestination(String destination) {
        this.destination = destination;
        return destination;
    }

    public String getFlightumber() {
        return flightumber;
    }

    public String setFlightumber(String flightumber) {
        this.flightumber = flightumber;
        return flightumber;
    }

    public String getTypeOfPlane() {
        return typeOfPlane;
    }

    public String setTypeOfPlane(String typeOfPlane) {
        this.typeOfPlane = typeOfPlane;
        return typeOfPlane;
    }

    public long getDispatchime() {
        return dispatchime;
    }

    public long setDispatchime(long dispatchime) {
        this.dispatchime = dispatchime;
        return dispatchime;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return dayOfWeek;
    }
    public boolean start () {


        final String [] destination = {"Kharkiv","Kiev","Dnipro","Lyviv","Kharkiv","Zaporijya","Kiev"};
        final String [] flightumber = {"A-984-CY","D-995-DF","T-878-RE","D-888-UY","H-123-KF","S-878-LK","H-897-MN"};
        final String [] typeOfPlane = {"TY-134","IL-62 ","AirBus A310","TY-134","AirBus A310","AirBus A310","Boeing-747"};
        final long dispatchime = 12;
        final String [] dayOfWeek = {"Monday","Tuesday","Friday","Thursday","Friday","Saturday","Sunday"};

        String userDestination = "Kharkiv";
        int userDispatchime = 5;
        String userDayOfWeek = "Friday";

        for (int i = 0; i < 7; i++) {
            AirLinesData airlinesMain = new AirLinesData(destination[i],flightumber[i],typeOfPlane[i],dispatchime + i,dayOfWeek[i]);
            String a = destination[i];
            if (a.contains(userDestination)) {
                System.out.println("By destination = " + airlinesMain.toString());
            }
        }

        for (int i = 0; i < 7; i++) {
            AirLinesData airlinesMain = new AirLinesData(destination[i],flightumber[i],typeOfPlane[i],dispatchime + i,dayOfWeek[i]);
            String a = dayOfWeek[i];
            if (a.contains(userDayOfWeek)) {
                System.out.println("By Day Of Week= " + airlinesMain.toString());
            }
        }

        for (int i = 0; i < 7; i++) {
            AirLinesData airlinesMain = new AirLinesData(destination[i],flightumber[i],typeOfPlane[i],dispatchime + i,dayOfWeek[i]);
            String a = dayOfWeek[i];
            if (a.contains(userDayOfWeek) && dispatchime > userDispatchime ) {
                System.out.println("By dispatchime = " + airlinesMain.toString());
            }
        }

        return true;
    }

}
