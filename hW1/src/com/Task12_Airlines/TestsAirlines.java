package com.Task12_Airlines;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsAirlines {

    AirLinesData airLinesData = new AirLinesData("Kharkiv","H-123-KF","AirBus A310",16,"Friday");

    @Test
    public void test1_Set_Get_Destination () {
        String actual = airLinesData.setDestination("Bora Bora");
        String expected = airLinesData.getDestination();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_Flightumber () {
        String actual = airLinesData.setFlightumber("as-878-s");
        String expected = airLinesData.getFlightumber();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Dispatchime() {
        long actual = airLinesData.setDispatchime(15);
        long expected = airLinesData.getDispatchime();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_DayOfWeek() {
        String actual = airLinesData.setDayOfWeek("Sunday");
        String expected = airLinesData.getDayOfWeek();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_toString () {
        String actual = "AirLinesData{destination='Kharkiv', flightumber='H-123-KF', typeOfPlane='AirBus A310', dispatchime=16, dayOfWeek='Friday'}";
        String expected = airLinesData.toString();
        Assertions.assertEquals(actual,expected);
    }
}

