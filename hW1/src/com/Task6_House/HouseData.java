package com.Task6_House;

import com.Task2_Customer.CustomerData;

public class HouseData {

    private int id;
    private int NumOfflat;
    private int area;
    private int floor;
    private int numOfRooms;
    private String street;
    private String typeOfBuilding;
    private String lifetime;

    public HouseData() {

    }

    @Override
    public String toString() {
        return "HouseData{" +
                "id=" + id +
                ", NumOfflat=" + NumOfflat +
                ", area=" + area +
                ", floor=" + floor +
                ", numOfRooms=" + numOfRooms +
                ", street='" + street + '\'' +
                ", typeOfBuilding='" + typeOfBuilding + '\'' +
                ", lifetime='" + lifetime + '\'' +
                '}';
    }



    public HouseData(int id, int numOfflat, int area, int floor, int numOfRoos, String street, String typeOfBuilding, String lifetime) {
        this.id = id;
        NumOfflat = numOfflat;
        this.area = area;
        this.floor = floor;
        this.numOfRooms = numOfRoos;
        this.street = street;
        this.typeOfBuilding = typeOfBuilding;
        this.lifetime = lifetime;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public int setNumOfflat(int numOfflat) {
        NumOfflat = numOfflat;
        return numOfflat;
    }

    public int setArea(int area) {
        this.area = area;
        return area;
    }

    public int setFloor(int floor) {
        this.floor = floor;
        return floor;
    }

    public int setNumOfRoos(int numOfRoos) {
        this.numOfRooms = numOfRoos;
        return numOfRoos;
    }

    public String setStreet(String street) {
        this.street = street;
        return street;
    }

    public String setTypeOfBuilding(String typeOfBuilding) {
        this.typeOfBuilding = typeOfBuilding;
        return typeOfBuilding;
    }

    public String setLifetime(String lifetime) {
        this.lifetime = lifetime;
        return lifetime;
    }

    public int getId() {
        return id;
    }

    public int getNumOfflat() {
        return NumOfflat;
    }

    public int getArea() {
        return area;
    }

    public int getFloor() {
        return floor;
    }

    public int getNumOfRoos() {
        return numOfRooms;
    }

    public String getStreet() {
        return street;
    }

    public String getTypeOfBuilding() {
        return typeOfBuilding;
    }

    public String getLifetime() {
        return lifetime;
    }

    public boolean start () {


        final int [] id = {529978};
        final int NumOfflat = 1;
        final int area = 42;
        final int floor = 1;
        final int numOfRooms = 0;
        final String [] street = {"Kharkiv,Lenina Ave, 74","Kharkiv,Nauky Ave, 68","Kharkiv,Lenina Ave, 66А","Kharkiv,Tobol's'ka St, 46Б","Kharkiv,Otakara Yarosha St, 21Б","Kharkiv,Varenykivs'kyi Ln, 14","Kharkiv,Dmytra Vyshnevetskoho St, 56","Kharkiv,Barykadnyi Ln, 2","Kharkiv,Alushtyns'ka St, 3","Kharkiv,Novocherkas'kyi Ln, 12","Kharkiv,Peremozhtsiv Ln, 14","Kharkiv,Poltavskyi Shliakh St, 188-10"};
        final String [] typeOfBuilding = {"Multi-storey building","House"};
        final String [] lifetime = {"50 years","60 years","30 years"};

        for (int i = 0; i < 6; i++) {

            HouseData numsOfRoom = new HouseData(id[0]+i,NumOfflat + i,area + (i +6),floor + i,numOfRooms ,street[i],typeOfBuilding[0],lifetime[0]);
            if (numOfRooms < 4){
                System.out.println("By rooms = " + numsOfRoom.toString());
            }
        }

        for (int i = 0; i < 6; i++) {
            HouseData roomsAndFloor = new HouseData(id[0]+i,NumOfflat + i,area + (i +6),floor +i ,numOfRooms+i,street[i],typeOfBuilding[0],lifetime[0]);
            if (numOfRooms < 3 && floor < 2){
                System.out.println("By rooms and floor = " + roomsAndFloor.toString());
            }
        }

        for (int i = 0; i < 6; i++) {
            HouseData roomsFloorArea = new HouseData(id[0]+i,NumOfflat + i,area + (i +6),floor ,numOfRooms ,street[i],typeOfBuilding[0],lifetime[0]);
            if (area < 80 && i % 2 == 0 ){
                roomsFloorArea.setLifetime(lifetime[1]);
                roomsFloorArea.setTypeOfBuilding(typeOfBuilding[0]);
                System.out.println("By area = " + roomsFloorArea.toString());
            } else if (area < 80 && i % 2 == 1) {
                roomsFloorArea.setLifetime(lifetime[2]);
                roomsFloorArea.setTypeOfBuilding(typeOfBuilding[1]);
                System.out.println("By area = " + roomsFloorArea.toString());
            }
        }

       return true;
    }


}
