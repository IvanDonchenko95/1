package com.Task6_House;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsHouse {

    HouseData houseData = new HouseData(529978,1,48,2,1,"Kharkiv,Lenina Ave, 74","Multi-storey building","60 years");

    @Test
    public void test1_Set_Get_ID () {
        int actual = houseData.setId(78);
        int expected = houseData.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_NumOfflat () {
        int actual = houseData.setNumOfflat(5);
        int expected = houseData.getNumOfflat();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Area () {
        int actual = houseData.setArea(68);
        int expected = houseData.getArea();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_Floor() {
        int actual = houseData.setFloor(2);
        int expected = houseData.getFloor();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_Get_NumOfRooms () {
        int actual = houseData.setNumOfRoos(2);
        int expected = houseData.getNumOfRoos();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_Streets() {
        String actual = houseData.setStreet("Kharkiv,Lenina Ave, 74");
        String expected = houseData.getStreet();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_TypeOfBuilding() {
        String actual = houseData.setTypeOfBuilding("House");
        String expected = houseData.getTypeOfBuilding();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test8_Set_Get_LifeTime() {
        String actual = houseData.setLifetime("45 year");
        String expected = houseData.getLifetime();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test9_toString () {
        String actual = "HouseData{id=529978, NumOfflat=1, area=48, floor=2, numOfRooms=1, street='Kharkiv,Lenina Ave, 74', typeOfBuilding='Multi-storey building', lifetime='60 years'}";
        String expected = houseData.toString();
        Assertions.assertEquals(actual,expected);
    }
}

