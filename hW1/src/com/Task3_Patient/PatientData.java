package com.Task3_Patient;

public class PatientData {

    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private String phone;
    private int cardNum;
    private String diagnosis;

    public PatientData() {
    }

    @Override
    public String toString() {
        return "PatientData{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", cardNum=" + cardNum +
                ", diagnosis='" + diagnosis + '\'' +
                '}';
    }

    public PatientData(int id, String surname, String name, String patronymic, String address, String phone, String diagnosis) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.phone = phone;
        this.diagnosis = diagnosis;
    }

    public PatientData(int id, String surname, String name, String patronymic, String address, String phone, int cardNum, String diagnosis) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.phone = phone;
        this.cardNum = cardNum;
        this.diagnosis = diagnosis;
    }

    public int getId() {
        return id;
    }

    public int setId(int id) {
        this.id = id;
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String setSurname(String surname) {
        this.surname = surname;
        return surname;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public String setAddress(String address) {
        this.address = address;
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String setPhone(String phone) {
        this.phone = phone;
        return phone;
    }

    public int getCardNum() {
        return cardNum;
    }

    public int setCardNum(int cardNum) {
        this.cardNum = cardNum;
        return cardNum;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
        return diagnosis;
    }

    public boolean start () {

        final int id = 3587;
        final String [] surname = {"Hryniuk","Hryhoruk","Mykhailuk","Yurskich","Tsar","Melnik","Shevchuk","Tkachenko","Moroz","Lysenko","Kolecnik","Juk"};
        final String [] name = {"Alexander","Anastasia","Daniel","Svyatoslav","Denis","German","Alice","Angelina","Lisa","Sophia","Maria","Natalie"};
        final String [] patronymic = {"Valeryevich","Valeryevna","Victorovich","Mikhaylovich","Vasilyevich","Victorovich","Nikolayevna","Mikhaylovna","Vladimirovna","Filipovna","Ivanovna","Fyodorovna"};
        final String [] address = {"Kharkiv,Lenina Ave, 74","Kharkiv,Nauky Ave, 68","Kharkiv,Lenina Ave, 66А","Kharkiv,Tobol's'ka St, 46Б","Kharkiv,Otakara Yarosha St, 21Б","Kharkiv,Varenykivs'kyi Ln, 14","Kharkiv,Dmytra Vyshnevetskoho St, 56","Kharkiv,Barykadnyi Ln, 2","Kharkiv,Alushtyns'ka St, 3","Kharkiv,Novocherkas'kyi Ln, 12","Kharkiv,Peremozhtsiv Ln, 14","Kharkiv,Poltavskyi Shliakh St, 188-10","Poltavskyi Shliakh St, 144"};
        final String [] phone = {"+38(099)274-74-56", "+38(099)625-67-75", "+38(099)235-53-60", "+38(099)537-91-63", "+38(099)380-83-79","+38(073)438-40-20","+38(073)032-66-28", "+38(073)795-32-51", "+38(098)460-68-29", "+38(098)933-46-67", "+38(098)417-78-86", "+38(098)977-87-73"};
        final String diagnosis = "headache";
        final int cardNum = 785;

        for (int i = 0; i < 4; i++) {
            PatientData patient2 = new PatientData(id+i,surname[i],name[i],patronymic[i],address[i],phone[i],cardNum+i,diagnosis);
            System.out.println("By diagnosis = " + patient2.toString());

        }

        for (int i = 0; i < 1; i++) {
            PatientData patient2 = new PatientData(id+1,surname[i],name[i],patronymic[i],address[i],phone[i],cardNum,diagnosis);
            System.out.println("By card num = " + patient2.toString());

        }

        return true;
    }
}
