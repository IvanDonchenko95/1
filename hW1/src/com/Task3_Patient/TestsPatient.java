package com.Task3_Patient;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsPatient {

    PatientData patient2  = new PatientData(3588,"Hryniuk","Alexander","Valeryevich","Kharkiv,Lenina Ave, 74","+38(099)274-74-56",785,"headache");

    @Test
    public void test1_Set_Get_ID () {
        int actual = patient2.setId(78);
        int expected = patient2.getId();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_Surname () {
        String actual = patient2.setSurname("Hryniuk");
        String expected = patient2.getSurname();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Name () {
        String actual = patient2.setName("Ivan");
        String expected = patient2.getName();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_Get_Patronymic () {
        String actual = patient2.setPatronymic("Vladimirovich");
        String expected = patient2.getPatronymic();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_Set_Get_Address () {
        String actual = patient2.setAddress("Kharkiv");
        String expected = patient2.getAddress();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Set_Get_PhoneNumber () {
        String actual = patient2.setPhone("+38(099)-225-65-89");
        String expected = patient2.getPhone();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_Set_Get_CardNum () {
        int actual = patient2.setCardNum(65);
        int expected = patient2.getCardNum();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test8_Set_Get_Course () {
        String actual = patient2.setDiagnosis("Pain");
        String expected = patient2.getDiagnosis();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test9_toString () {
        String actual = "PatientData{id=3588, surname='Hryniuk', name='Alexander', patronymic='Valeryevich', address='Kharkiv,Lenina Ave, 74', phone='+38(099)274-74-56', cardNum=785, diagnosis='headache'}";
        String expected = patient2.toString();
        Assertions.assertEquals(actual,expected);
    }

}
