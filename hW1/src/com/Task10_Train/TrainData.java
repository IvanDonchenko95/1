package com.Task10_Train;

public class TrainData {

    private String destination;
    private String trainNum;
    private int dispatchime;
    private int numOfSats;

    public TrainData(String destination, String trainNim, int dispatchime, int numOfSats) {
        this.destination = destination;
        this.trainNum = trainNim;
        this.dispatchime = dispatchime;
        this.numOfSats = numOfSats;
    }

    public TrainData() {

    }

    @Override
    public String toString() {
        return "TrainData{" +
                "destination='" + destination + '\'' +
                ", trainNim='" + trainNum + '\'' +
                ", dispatchime=" + dispatchime +
                ", numOfSats=" + numOfSats +
                '}';
    }

    public String getDestination() {
        return destination;
    }

    public String setDestination(String destination) {
        this.destination = destination;
        return destination;
    }

    public String getTrainNum() {
        return trainNum;
    }

    public String setTrainNum(String trainNim) {
        this.trainNum = trainNim;
        return trainNim;
    }

    public int getDispatchime() {
        return dispatchime;
    }

    public int setDispatchime(int dispatchime) {
        this.dispatchime = dispatchime;
        return dispatchime;
    }

    public int getNumOfSats() {
        return numOfSats;
    }

    public int setNumOfSats(int numOfSats) {
        this.numOfSats = numOfSats;
        return numOfSats;
    }

    public boolean start () {

        final String [] destination = {"Kharkiv","Kiev","Dnipro","Lyviv","Kharkiv","Zaporijya","Native town","Kiev"};
        final String[] trainNum = {"A-984-CY","D-995-DF","T-878-RE","D-888-UY","H-123-KF","S-878-LK","H-897-MN","Y-565-KJ"};
        final int dispatchime = 12;
        final int numOfSats = 183;

        String userDestination = "Kharkiv";
        int userDispatchime = 5;
        int userNumOfSats = 254;

        for (int i = 0; i < 8; i++) {
            TrainData trainMain = new TrainData(destination[i],trainNum[i],dispatchime + i, numOfSats - i );
            String a = destination[i];
            if (a.contains("Kiev")) {
                System.out.println("By destination = " + trainMain.toString());
            }
        }
        for (int i = 0; i < 8; i++) {
            TrainData trainMain = new TrainData(destination[i],trainNum[i],dispatchime + i, numOfSats - i);
            String a = destination[i];
            if (a.contains(userDestination) && dispatchime > userDispatchime) {
                System.out.println("By dispatchime = " + trainMain.toString());
            }
        }

        for (int i = 0; i < 8; i++) {
            TrainData trainMain = new TrainData(destination[i],trainNum[i],dispatchime + i, numOfSats - i);
            String a = destination[i];
            if (a.contains(userDestination) && numOfSats < userNumOfSats) {
                System.out.println("By num Of sats = " + trainMain.toString());
            }
        }

        return true;
    }
}
