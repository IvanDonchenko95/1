package com.Task10_Train;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsTrain {

    TrainData trainData = new TrainData("Kharkiv","H-123-KF" ,16,179);

    @Test
    public void test1_Set_Get_Destination () {
        String actual = trainData.setDestination("Bora Bora");
        String expected = trainData.getDestination();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Set_Get_TrainNum () {
        String actual = trainData.setTrainNum("Ivan");
        String expected = trainData.getTrainNum();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Set_Get_Dispatchime () {
        int actual = trainData.setDispatchime(12);
        int expected = trainData.getDispatchime();
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Set_NumOfSats() {
        int actual = trainData.setNumOfSats(120);
        int expected = trainData.getNumOfSats();
        Assertions.assertEquals(actual,expected);
    }


    @Test
    public void test5_toString () {
        String actual = "TrainData{destination='Kharkiv', trainNim='H-123-KF', dispatchime=16, numOfSats=179}";
        String expected = trainData.toString();
        Assertions.assertEquals(actual,expected);
    }
}
