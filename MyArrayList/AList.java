package com.MyArrayList;

public class AList implements IList{

    private int defaultCapacity = 10;
    private int[] array;
    private int size;
    private int index;
    private int pointer = 0;

    public AList () {
        this.array = new int[defaultCapacity];
    }

    public AList (int capacity) {
        if (capacity < defaultCapacity) {
            this.array = new int[capacity];
        }
    }

    public AList(int[] arr) {
        this();
        if (arr.length < defaultCapacity) {
            for (int i = 0; i < arr.length; i++) {
                add(arr[i]);
            }
        } else if (arr.length >= defaultCapacity) {
            growArray();
            for (int i = 0; i < arr.length; i++) {
                add(arr[i]);
            }
        }
    }




    @Override
    public int length() {
        return this.array.length;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        return this.array[index];
    }

    @Override
    public boolean add(int value) {

        if (index == array.length) {
            growArray();
        }
        array[index] = value;
        index++;
        size++;
        return true;
    }

    @Override
    public boolean add(int index, int value) {
            checkIndex(index);
        if (index == array.length) {
            growArray();
        }
        System.arraycopy(array,index,array,index+1,this.size-index);
        array[index] = value;
        size++;
        return true;
    }

    @Override
    public boolean remove(int number) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (number != array[i]) {
                newArray[i] = array[i];
            } else if (number == array[i]) {
                System.arraycopy(array,i+1,array,i,size-i);
                size--;
                index--;
            }
        }

        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                System.arraycopy(array,i+1,array,i,size-i);
                size--;
                index--;
            }
        }

        if (size < 10) {
            newArray = new int[defaultCapacity];
        } else {
            newArray = new int[array.length];
        }

        for (int i = 0; i < newArray.length - 1; i++) {
            newArray[i] = array[i];
        }

        array = newArray;

        return true;
    }

    @Override
    public int removeByIndex(int index) {
        checkIndex(index);
        System.arraycopy(array,index+1,array,index,this.index - index);
        size--;
        this.index--;
        return index;
    }

    @Override
    public boolean growArray() {
        int[] newArray = new int[(int) (array.length * 1.5)];
        System.arraycopy(array, 0, newArray, 0, index);
        array = newArray;
        return true;
    }

    @Override
    public boolean set(int index, int value) {
        checkIndex(index);
        array[index] = value;

        return true;
    }

    @Override
    public String print() {
        String num = "";
        for (int i = 0; i < array.length; i++) {
            num += array[i] + " , ";

        }
        return "[ " + num.substring(0,num.length()-2) + "]";
    }

    @Override
    public int[] toArray() {
        int[] newarray = this.array;
        return newarray;
    }


    @Override
    public boolean removeAll(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }
        return true;
    }


    private void checkIndex(int index) {
        if (index < 0 || index >= this.index)
            throw new IllegalArgumentException();
    }

}
