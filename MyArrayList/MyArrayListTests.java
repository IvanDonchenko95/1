package com.MyArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MyArrayListTests {
    final int[] array1 = {54,45,54,2,3,5,98,7,5,12,32,21,121};
    final int[] array2 = {54,2,32,7,1,2};
    AList aList = new AList();

    @Test
    public void test1_Add_32_toTheArray () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        aList.add(32);
        String actual1 = aList.print();
        String expected = "[ 54 , 2 , 32 , 7 , 1 , 2 , 32 , 0 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test2_Add_32_and_57_toTheArray () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        aList.add(32);
        aList.add(57);
        String actual1 = aList.print();
        String expected = "[ 54 , 2 , 32 , 7 , 1 , 2 , 32 , 57 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test3_AddByIndex_index_3_val_32_toTheArray () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        aList.add(3,32);
        String actual1 = aList.print();
        String expected = "[ 54 , 2 , 32 , 32 , 7 , 1 , 2 , 0 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test4_Size_of_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        int actual1 = aList.size();
        int expected = 6;
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test5_Length_of_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        int actual1 = aList.length();
        int expected = 10;
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test6_Remove_32_from_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        boolean actual = aList.remove(32);
        String actual1 = aList.print();
        String expected = "[ 54 , 2 , 7 , 1 , 2 , 0 , 0 , 0 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test7_Remove_32_from_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        boolean actual = aList.removeAll(new int[] {32,2});
        String actual1 = aList.print();
        String expected = "[ 54 , 7 , 1 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test8_RemoveByIndex_index_0_from_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        int actual = aList.removeByIndex(0);
        String actual1 = aList.print();
        String expected = "[ 2 , 32 , 7 , 1 , 2 , 0 , 0 , 0 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test9_Set_index_0_val_5_toArray () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        boolean actual = aList.set(1,5);
        String actual1 = aList.print();
        String expected = "[ 54 , 5 , 32 , 7 , 1 , 2 , 0 , 0 , 0 , 0 ]";
        Assertions.assertEquals(actual1,expected);

    }

    @Test
    public void test10_Get_index_5_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }
        int actual = aList.get(5);
        int expected = 2;
        Assertions.assertEquals(actual,expected);

    }
    @Test
    public void test11_toArray_Array () {
        for (int i = 0; i < array2.length; i++) {
            aList.add(array2[i]);
        }

        int[] actual = aList.toArray();
        int[] expected = aList.toArray();
        Assertions.assertEquals(actual,expected);

    }

}
