package com.MyArrayList;

public interface IList {

    int length();

    int size();
    int get(int index);
    boolean add (int value);
    boolean add (int index,int value);
    boolean remove (int number);
    int removeByIndex (int index);
    boolean growArray ();
    boolean set (int index, int value);
    String print ();
    int[] toArray ();
    boolean removeAll(int[] arr);

}
