
import org.junit.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ConverterConfig {

    public static ChromeDriver chrome;

    protected String convBott = "btnConvert";
    protected String input = "inputNumber";
    protected String out = "inputResult";
    protected String convertFrom = "covertFrom";
    protected String convertTo = "covertTo";
    protected String versts = "[value = 'versts']";
    protected String meters = "[value = 'meters']";
    protected String yards = "[value = 'yards']";
    protected String miles = "[value = 'miles']";
    protected String foots = "[value = 'foots']";

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Driver\\chromedriver.exe");
        chrome = new ChromeDriver();
        chrome.get("http://34.90.36.71/apps/anrzhievskyi/converterLength/index.html");
    }

    @AfterAll
    public static void close () {
        chrome.quit();
    }
}

