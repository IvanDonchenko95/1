import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class ConverterTests extends ConverterConfig{


    @Test
    public void test1_Integer_Num_63_FromVerst_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");
        Assertions.assertEquals("63.0000",rst);
        chrome.close();

    }

    @Test
    public void test2_Fractional_NumFrom_63and1_Verst_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");
        Assertions.assertEquals("63.1000",rst);
        chrome.close();
    }
    @Test
    public void test3_Integer_Num_63_FromVerst_to_Meters (){
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("67208.4000",rst);
        chrome.close();
    }

    @Test
    public void test4_Fractional_NumFrom_63and1_Verst_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("67315.0800",rst);
        chrome.close();

    }

    @Test
    public void test5_Integer_Num_63_FromVerst_to_Yards (){
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("73500.0021",rst);
        chrome.close();
    }

    @Test
    public void test6_Fractional_NumFrom_63and1_Verst_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("73616.6667",rst);
        chrome.close();
    }

    @Test
    public void test7_Integer_Num_63_FromVerst_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("41.7614",rst);
        chrome.close();
    }

    @Test
    public void test8_Fractional_NumFrom_63and1_Verst_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("41.8277",rst);
        chrome.close();
    }

    @Test
    public void test8_Integer_Num_63_FromVerst_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("220500.0000",rst);
        chrome.close();
    }

    @Test
    public void test9_Fractional_NumFrom_63and1_Verst_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(versts)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("220850.0000",rst);
        chrome.close();
    }

    @Test
    public void test10_Integer_Num_63_FromMeters_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("0.0591",rst);
        chrome.close();
    }

    @Test
    public void test11_Fractional_NumFrom_63and1_Meters_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("0.0591",rst);
        chrome.close();
    }

    @Test
    public void test12_Integer_Num_63_FromMeters_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("63.0000",rst);
        chrome.close();
    }

    @Test
    public void test13_Fractional_NumFrom_63and1_Meters_to_Meters (){
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("63.1000",rst);
        chrome.close();
    }

    @Test
    public void test14_Integer_Num_63_FromMeters_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("68.8977",rst);
        chrome.close();
    }

    @Test
    public void test15_Fractional_NumFrom_63and1_Meters_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("69.0070",rst);
        chrome.close();
    }

    @Test
    public void test16_Integer_Num_63_FromMeters_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("0.0391",rst);
        chrome.close();
    }

    @Test
    public void test17_Fractional_NumFrom_63and1_Meters_to_Miles() {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("0.0392",rst);
        chrome.close();
    }

    @Test
    public void test18_Integer_Num_63_FromMeters_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("206.6930",rst);
        chrome.close();
    }

    @Test
    public void test19_Fractional_NumFrom_63and1_Meters_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(meters)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("207.0230",rst);
        chrome.close();
    }

    @Test
    public void test20_Integer_Num_63_FromYards_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("0.0540",rst);
        chrome.close();
    }

    @Test
    public void test21_Fractional_NumFrom_63and1_Yards_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("0.0541",rst);
        chrome.close();
    }

    @Test
    public void test22_Integer_Num_63_FromYards_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("57.6072",rst);
        chrome.close();
    }

    @Test
    public void test23_Fractional_NumFrom_63and1_FromYards_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("57.6986",rst);
        chrome.close();
    }

    @Test
    public void test24_Integer_Num_63_FromYards_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("63.0000",rst);
        chrome.close();
    }

    @Test
    public void test25_Fractional_NumFrom_63and1_FromYards_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("63.1000",rst);
        chrome.close();
    }

    @Test
    public void test26_Integer_Num_63_FromYards_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("0.0358",rst);
        chrome.close();
    }

    @Test
    public void test27_Fractional_NumFrom_63and1_FromYards_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("0.0358",rst);
        chrome.close();
    }

    @Test
    public void test28_Integer_Num_63_FromYards_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("189.0000",rst);
        chrome.close();
    }

    @Test
    public void test29_Fractional_NumFrom_63and1_FromYards_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(yards)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("189.3000",rst);
        chrome.close();
    }

    @Test
    public void test30_Integer_Num_63_FromMiles_to_Verst (){
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("95.0400",rst);
        chrome.close();
    }

    @Test
    public void test31_Fractional_NumFrom_63and1_FromMiles_to_Verst () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("95.1908",rst);
        chrome.close();
    }

    @Test
    public void test32_Integer_Num_63_FromMiles_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("101388.6720",rst);
        chrome.close();
    }

    @Test
    public void test33_Fractional_NumFrom_63and1_FromMiles_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("101549.6064",rst);
        chrome.close();
    }

    @Test
    public void test34_Integer_Num_63_FromMiles_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("110880.0000",rst);
        chrome.close();
    }

    @Test
    public void test35_Fractional_NumFrom_63and1_FromMiles_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("111056.0000",rst);
        chrome.close();
    }

    @Test
    public void test36_Integer_Num_63_FromMiles_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("63.0000",rst);
        chrome.close();
    }

    @Test
    public void test37_Fractional_NumFrom_63and1_FromMiles_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("63.1000",rst);
        chrome.close();
    }

    @Test
    public void test38_Integer_Num_63_FromMiles_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("332640.0000",rst);
        chrome.close();
    }

    @Test
    public void test39_Fractional_NumFrom_63and1_FromMiles_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(miles)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("333168.0000",rst);
        chrome.close();
    }

    @Test
    public void test40_Integer_Num_63_FromFoots_to_Versts () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("0.0180",rst);
        chrome.close();
    }

    @Test
    public void test41_Fractional_NumFrom_63and1_FromFoots_to_Versts () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(versts)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("0.0180",rst);
        chrome.close();
    }

    @Test
    public void test42_Integer_Num_63_FromFoots_to_Metres () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("19.2024",rst);
        chrome.close();
    }

    @Test
    public void test43_Fractional_NumFrom_63and1_FromFoots_to_Meters () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(meters)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("19.2323",rst);
        chrome.close();
    }

    @Test
    public void test44_Integer_Num_63_FromFoots_to_Yards () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("21.0000",rst);
        chrome.close();
    }

    @Test
    public void test45_Fractional_NumFrom_63and1_FromFoots_to_Yards (){
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(yards)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("21.0333",rst);
        chrome.close();
    }

    @Test
    public void test46_Integer_Num_63_FromFoots_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("0.0119",rst);
        chrome.close();
    }

    @Test
    public void test47_Fractional_NumFrom_63and1_FromFoots_to_Miles () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(miles)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("0.0120",rst);
        chrome.close();
    }

    @Test
    public void test48_Integer_Num_63_FromFoots_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");

        Assertions.assertEquals("63.0000",rst);
        chrome.close();
    }

    @Test
    public void test49_Fractional_NumFrom_63and1_FromFoots_to_Foots () {
        WebElement selectFrom = chrome.findElement(new By.ById(convertFrom));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector(foots)).click();
        WebElement inputField = chrome.findElement(new By.ById(input));
        inputField.sendKeys("63.1");
        WebElement selectTo = chrome.findElement(new By.ById(convertTo));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector(foots)).click();

        WebElement res  = chrome.findElement(new By.ById(convBott));
        res.click();

        String rst = chrome.findElement(new By.ById(out)).getAttribute("value");


        Assertions.assertEquals("63.1000",rst);
        chrome.close();
    }
}
