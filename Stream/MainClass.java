package com.Stream;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class MainClass {


    public static void main(String[] args) {
        System.out.println(Streams.findAll());
        System.out.println("By Faculty: " + "\n" +Streams.getFaculty(Streams.findAll(),"PM"));
        System.out.println("By Faculty and Course: " + "\n" + Streams.getFacultyAndCourse(Streams.findAll(),"PM","I"));
        System.out.println("ByYear: " + "\n" + Streams.byYear(Streams.findAll(),1995));
        System.out.println("Students in format LastName, FirstName, Patronymic: " + "\n" + Streams.students(Streams.findAll()));
        System.out.println("Count: " + "\n" + Streams.findAll().stream().count());
        System.out.println("From old to New Faculty: " + "\n" + Streams.toFaulty(Streams.findAll(),"JAVA"));
        System.out.println("From old to New Group: " + "\n" +Streams.toGroup(Streams.findAll(),"AT"));
        System.out.println("Students in format LastName, FirstName, Patronymic, Faculty, Group: " + " \n " + Streams.getReduce(Streams.findAll(),4,"\n"));

    }

}


//    public static List <Students> allStud () {
//
//        List<Students> students = new ArrayList<>();
//
//        for (int i = 0; i < 9; i++) {
//            if (i % 2 == 0) {
//                Students stud = new Students(i+1,firstName[i],patronymic [i],lastName [i],yearOfBirth [i],address [i],phone [i],faculty[0],course[i],group[0]);
//                students.add(stud);
//            } else {
//                Students stud = new Students(i+1,firstName[i],patronymic [i],lastName [i],yearOfBirth [i],address [i],phone [i],faculty[1],course[i],group[1]);
//                students.add(stud);
//            }
//        }
//
//        return students;
//    }


//    public static Stream<Students> getFaculty (List <Students> collect,String faculty) {
//        Stream<Students> firstStream = collect.stream();
//        firstStream.map((s)->{
//            s.setFaculty(faculty);
//            return "id: " +s.getId()+ " faculty: " +s.getFaculty()+ " student: "+s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName() + ".";
//        }).forEach((s) -> System.out.println(s));
//        return firstStream;
//    }
//
//    public static Stream<Students> getFacultyAndCourse (List <Students> collect,int courseNum,String faculty) {
//        Stream<Students> firstStream = collect.stream();
//        firstStream.map((s)->{
//            s.setCourse(course[courseNum]);
//            s.setFaculty(faculty);
//            return "id: " +s.getId() + " course " + s.getCourse() + " faculty: "+s.getFaculty()+" student: "+s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName() + ".";
//        }).forEach((s) -> System.out.println(s));
//        return firstStream;
//    }
//
//    public static Stream<Students> getYear (List <Students> collect,int year) {
//        Stream<Students> firstStream = collect.stream();
//        firstStream.map((s)->{
//            s.setYearOfBirth(year);
//            return "id: " +s.getId() + " year: " + s.getYearOfBirth() + " course " + s.getCourse() + " faculty: "+s.getFaculty()+" student: "+s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName() + ".";
//        }).forEach((s) -> System.out.println(s));
//        return firstStream;
//    }
//
//    public static Stream<Students> getGroup (List <Students> collect,String group) {
//        Stream<Students> firstStream = collect.stream();
//        firstStream.map((s)->{
//            s.setGroup(group);
//            return "id: " +s.getId() + " group: " + s.getGroup() + " student: "+s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName() + ".";
//        }).forEach((s) -> System.out.println(s));
//        return firstStream;
//    }
//    public static String countFaculty (List <Students> collect) {
//        long firstStream = collect.stream().count();
//
//        return "Count is: " + firstStream;
//    }
//
//    public static Stream<Students> toGroup (List <Students> collect,String group) {
//        Stream<Students> firstStream = collect.stream();
//        firstStream.map((s)->{
//            return "id: " +s.getId() + " old group: " + s.getGroup() + " student: " + s.getFirstName() + " " + s.getPatronymic() + " " + s.getLastName() + ".";
//
//        }).forEach((s) -> System.out.println(s));
//        Stream<Students> secondStream = collect.stream();
//        secondStream.map((e)->{
//            e.setGroup(group);
//            return "\n" + "id: " +e.getId() + " new group: " + e.getGroup() + " student: " + e.getFirstName() + " " + e.getPatronymic() + " " + e.getLastName() + ".";
//        }).forEach((e) -> System.out.println(e));
//        return secondStream;
//    }

