package com.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StreamsTests {


    @Test
    public void test1_CreateCollection () {

        String actual = Streams.findAll().toString();
        String exepted = "[Students{ID = 1, FirstName = 'Hryniuk', LastName = 'Valeryevich', Patronymic = 'Alexander', YearOfBirth = 1996, Address = 'Kharkiv,Lenina Ave, 74', Telephone = '+38(099)274-74-56', Faculty = 'PM', Course = 'I', Group = 'QA'}\n" +
                ", Students{ID = 2, FirstName = 'Hryhoruk', LastName = 'Valeryevna', Patronymic = 'Anastasia', YearOfBirth = 1997, Address = 'Kharkiv,Nauky Ave, 68', Telephone = '+38(099)625-67-75', Faculty = 'IT', Course = 'III', Group = 'AT'}\n" +
                ", Students{ID = 3, FirstName = 'Mykhailuk', LastName = 'Victorovich', Patronymic = 'Daniel', YearOfBirth = 1995, Address = 'Kharkiv,Lenina Ave, 66А', Telephone = '+38(099)235-53-60', Faculty = 'IT', Course = 'II', Group = 'QA'}\n" +
                ", Students{ID = 4, FirstName = 'Yurskich', LastName = 'Mikhaylovich', Patronymic = 'Svyatoslav', YearOfBirth = 1997, Address = 'Kharkiv,Tobol's'ka St, 46Б', Telephone = '+38(099)537-91-63', Faculty = 'IT', Course = 'I', Group = 'AT'}\n" +
                ", Students{ID = 5, FirstName = 'Tsar', LastName = 'Vasilyevich', Patronymic = 'Denis', YearOfBirth = 1995, Address = 'Kharkiv,Otakara Yarosha St, 21Б', Telephone = '+38(099)380-83-79', Faculty = 'PM', Course = 'IV', Group = 'QA'}\n" +
                ", Students{ID = 6, FirstName = 'Melnik', LastName = 'Victorovich', Patronymic = 'German', YearOfBirth = 1997, Address = 'Kharkiv,Varenykivs'kyi Ln, 14', Telephone = '+38(073)438-40-20', Faculty = 'PM', Course = 'II', Group = 'QA'}\n" +
                ", Students{ID = 7, FirstName = 'Shevchuk', LastName = 'Nikolayevna', Patronymic = 'Alice', YearOfBirth = 1996, Address = 'Kharkiv,Dmytra Vyshnevetskoho St, 56', Telephone = '+38(073)032-66-28', Faculty = 'IT', Course = 'III', Group = 'AT'}\n" +
                ", Students{ID = 8, FirstName = 'Tkachenko', LastName = 'Mikhaylovna', Patronymic = 'Angelina', YearOfBirth = 1995, Address = 'Kharkiv,Barykadnyi Ln, 2', Telephone = '+38(073)795-32-51', Faculty = 'IT', Course = 'V', Group = 'QA'}\n" +
                ", Students{ID = 9, FirstName = 'Moroz', LastName = 'Vladimirovna', Patronymic = 'Lisa', YearOfBirth = 1997, Address = 'Kharkiv,Alushtyns'ka St, 3', Telephone = '+38(098)460-68-29', Faculty = 'PM', Course = 'II', Group = 'AT'}\n" +
                ", Students{ID = 10, FirstName = 'Lysenko', LastName = 'Filipovna', Patronymic = 'Sophia', YearOfBirth = 1998, Address = 'Kharkiv,Novocherkas'kyi Ln, 12', Telephone = '+38(098)933-46-67', Faculty = 'IT', Course = 'III', Group = 'AT'}\n" +
                "]";
        Assertions.assertEquals(actual,exepted);
    }

    @Test
    public void test2_GetFaculty () {

        String actual = Streams.getFaculty(Streams.findAll(), "PM").toString();
        String expected = "[Students{ID = 1, FirstName = 'Hryniuk', LastName = 'Valeryevich', Patronymic = 'Alexander', YearOfBirth = 1996, Address = 'Kharkiv,Lenina Ave, 74', Telephone = '+38(099)274-74-56', Faculty = 'PM', Course = 'I', Group = 'QA'}\n" +
                ", Students{ID = 5, FirstName = 'Tsar', LastName = 'Vasilyevich', Patronymic = 'Denis', YearOfBirth = 1995, Address = 'Kharkiv,Otakara Yarosha St, 21Б', Telephone = '+38(099)380-83-79', Faculty = 'PM', Course = 'IV', Group = 'QA'}\n" +
                ", Students{ID = 6, FirstName = 'Melnik', LastName = 'Victorovich', Patronymic = 'German', YearOfBirth = 1997, Address = 'Kharkiv,Varenykivs'kyi Ln, 14', Telephone = '+38(073)438-40-20', Faculty = 'PM', Course = 'II', Group = 'QA'}\n" +
                ", Students{ID = 9, FirstName = 'Moroz', LastName = 'Vladimirovna', Patronymic = 'Lisa', YearOfBirth = 1997, Address = 'Kharkiv,Alushtyns'ka St, 3', Telephone = '+38(098)460-68-29', Faculty = 'PM', Course = 'II', Group = 'AT'}\n" +
                "]";
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_GetFacultyAndCourse () {

        String actual = Streams.getFacultyAndCourse(Streams.findAll(),"PM","I").toString();
        String exepted = "[Students{ID = 1, FirstName = 'Hryniuk', LastName = 'Valeryevich', Patronymic = 'Alexander', YearOfBirth = 1996, Address = 'Kharkiv,Lenina Ave, 74', Telephone = '+38(099)274-74-56', Faculty = 'PM', Course = 'I', Group = 'QA'}\n" +
                "]";
        Assertions.assertEquals(actual,exepted);
    }

    @Test
    public void test4_ByYear () {

        String actual = Streams.byYear(Streams.findAll(),1995).toString();
        String expected = "[Students{ID = 1, FirstName = 'Hryniuk', LastName = 'Valeryevich', Patronymic = 'Alexander', YearOfBirth = 1996, Address = 'Kharkiv,Lenina Ave, 74', Telephone = '+38(099)274-74-56', Faculty = 'PM', Course = 'I', Group = 'QA'}\n" +
                ", Students{ID = 2, FirstName = 'Hryhoruk', LastName = 'Valeryevna', Patronymic = 'Anastasia', YearOfBirth = 1997, Address = 'Kharkiv,Nauky Ave, 68', Telephone = '+38(099)625-67-75', Faculty = 'IT', Course = 'III', Group = 'AT'}\n" +
                ", Students{ID = 4, FirstName = 'Yurskich', LastName = 'Mikhaylovich', Patronymic = 'Svyatoslav', YearOfBirth = 1997, Address = 'Kharkiv,Tobol's'ka St, 46Б', Telephone = '+38(099)537-91-63', Faculty = 'IT', Course = 'I', Group = 'AT'}\n" +
                ", Students{ID = 6, FirstName = 'Melnik', LastName = 'Victorovich', Patronymic = 'German', YearOfBirth = 1997, Address = 'Kharkiv,Varenykivs'kyi Ln, 14', Telephone = '+38(073)438-40-20', Faculty = 'PM', Course = 'II', Group = 'QA'}\n" +
                ", Students{ID = 7, FirstName = 'Shevchuk', LastName = 'Nikolayevna', Patronymic = 'Alice', YearOfBirth = 1996, Address = 'Kharkiv,Dmytra Vyshnevetskoho St, 56', Telephone = '+38(073)032-66-28', Faculty = 'IT', Course = 'III', Group = 'AT'}\n" +
                ", Students{ID = 9, FirstName = 'Moroz', LastName = 'Vladimirovna', Patronymic = 'Lisa', YearOfBirth = 1997, Address = 'Kharkiv,Alushtyns'ka St, 3', Telephone = '+38(098)460-68-29', Faculty = 'PM', Course = 'II', Group = 'AT'}\n" +
                ", Students{ID = 10, FirstName = 'Lysenko', LastName = 'Filipovna', Patronymic = 'Sophia', YearOfBirth = 1998, Address = 'Kharkiv,Novocherkas'kyi Ln, 12', Telephone = '+38(098)933-46-67', Faculty = 'IT', Course = 'III', Group = 'AT'}\n" +
                "]";
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test5_InFormat () {

        String actual = Streams.students(Streams.findAll()).toString();
        String expected = "[ID: 1, Student: Hryniuk Alexander Valeryevich\n" +
                ", ID: 2, Student: Hryhoruk Anastasia Valeryevna\n" +
                ", ID: 3, Student: Mykhailuk Daniel Victorovich\n" +
                ", ID: 4, Student: Yurskich Svyatoslav Mikhaylovich\n" +
                ", ID: 5, Student: Tsar Denis Vasilyevich\n" +
                ", ID: 6, Student: Melnik German Victorovich\n" +
                ", ID: 7, Student: Shevchuk Alice Nikolayevna\n" +
                ", ID: 8, Student: Tkachenko Angelina Mikhaylovna\n" +
                ", ID: 9, Student: Moroz Lisa Vladimirovna\n" +
                ", ID: 10, Student: Lysenko Sophia Filipovna\n" +
                "]";
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Count () {

        long actual = Streams.findAll().stream().count();
        long expected = 10;
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test7_DifferentFacult () {

        String actual = Streams.toFaulty(Streams.findAll(),"JAVA").toString();
        String expected = "[ID: 1, Faculty: JAVA, Student: Hryniuk Alexander Valeryevich\n" +
                ", ID: 2, Faculty: JAVA, Student: Hryhoruk Anastasia Valeryevna\n" +
                ", ID: 3, Faculty: JAVA, Student: Mykhailuk Daniel Victorovich\n" +
                ", ID: 4, Faculty: JAVA, Student: Yurskich Svyatoslav Mikhaylovich\n" +
                ", ID: 5, Faculty: JAVA, Student: Tsar Denis Vasilyevich\n" +
                ", ID: 6, Faculty: JAVA, Student: Melnik German Victorovich\n" +
                ", ID: 7, Faculty: JAVA, Student: Shevchuk Alice Nikolayevna\n" +
                ", ID: 8, Faculty: JAVA, Student: Tkachenko Angelina Mikhaylovna\n" +
                ", ID: 9, Faculty: JAVA, Student: Moroz Lisa Vladimirovna\n" +
                ", ID: 10, Faculty: JAVA, Student: Lysenko Sophia Filipovna\n" +
                "]";
        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void test8_DifferentGroup () {

        String actual = Streams.toGroup(Streams.findAll(),"AT").toString();
        String expected = "[ID: 1, Group: AT, Student: Hryniuk Alexander Valeryevich\n" +
                ", ID: 2, Group: AT, Student: Hryhoruk Anastasia Valeryevna\n" +
                ", ID: 3, Group: AT, Student: Mykhailuk Daniel Victorovich\n" +
                ", ID: 4, Group: AT, Student: Yurskich Svyatoslav Mikhaylovich\n" +
                ", ID: 5, Group: AT, Student: Tsar Denis Vasilyevich\n" +
                ", ID: 6, Group: AT, Student: Melnik German Victorovich\n" +
                ", ID: 7, Group: AT, Student: Shevchuk Alice Nikolayevna\n" +
                ", ID: 8, Group: AT, Student: Tkachenko Angelina Mikhaylovna\n" +
                ", ID: 9, Group: AT, Student: Moroz Lisa Vladimirovna\n" +
                ", ID: 10, Group: AT, Student: Lysenko Sophia Filipovna\n" +
                "]" ;
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test9_GetReduce () {

        String actual = Streams.getReduce(Streams.findAll(),4,"\n").toString();
        String expected = "Optional[ID: 1, Student: Hryniuk Alexander Valeryevich, Faculty: PM, Group: QA\n" +
                "\n" +
                "ID: 2, Student: Hryhoruk Anastasia Valeryevna, Faculty: IT, Group: AT\n" +
                "\n" +
                "ID: 3, Student: Mykhailuk Daniel Victorovich, Faculty: IT, Group: QA\n" +
                "\n" +
                "ID: 4, Student: Yurskich Svyatoslav Mikhaylovich, Faculty: IT, Group: AT\n" +
                "]";

        Assertions.assertEquals(actual,expected);

    }
}
