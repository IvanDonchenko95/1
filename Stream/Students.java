package com.Stream;
public class Students {

    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private int yearOfBirth;
    private String address;
    private String telephone;
    private String faculty;
    private String course;
    private String group;

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getCourse() {
        return course;
    }

    public String getGroup() {
        return group;
    }

    Students (int id, String firstName,String patronymic, String lastName, int yearOfBirth, String address, String telephone, String faculty, String course, String group) {
        this.id = id;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
        this.address = address;
        this.telephone = telephone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Students{" +
                "ID = " + id +
                ", FirstName = '" + firstName + '\'' +
                ", LastName = '" + lastName + '\'' +
                ", Patronymic = '" + patronymic + '\'' +
                ", YearOfBirth = " + yearOfBirth +
                ", Address = '" + address + '\'' +
                ", Telephone = '" + telephone + '\'' +
                ", Faculty = '" + faculty + '\'' +
                ", Course = '" + course + '\'' +
                ", Group = '" + group + '\'' +
                "}" + "\n";
    }

}
