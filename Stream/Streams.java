package com.Stream;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Streams {
    final static int[] id = {1,2,3,4,5,6,7,8,9,10,11,12};
    final static String [] lastName = {"Hryniuk","Hryhoruk","Mykhailuk","Yurskich","Tsar","Melnik","Shevchuk","Tkachenko","Moroz","Lysenko","Kolecnik","Juk"};
    final static String [] firstName = {"Alexander","Anastasia","Daniel","Svyatoslav","Denis","German","Alice","Angelina","Lisa","Sophia","Maria","Natalie"};
    final static String [] patronymic = {"Valeryevich","Valeryevna","Victorovich","Mikhaylovich","Vasilyevich","Victorovich","Nikolayevna","Mikhaylovna","Vladimirovna","Filipovna","Ivanovna","Fyodorovna"};
    final static int [] yearOfBirth = {1996,1997,1995,1997,1995,1997,1996,1995,1997,1998,1997,1996};
    final static String [] address = {"Kharkiv,Lenina Ave, 74","Kharkiv,Nauky Ave, 68","Kharkiv,Lenina Ave, 66А","Kharkiv,Tobol's'ka St, 46Б","Kharkiv,Otakara Yarosha St, 21Б","Kharkiv,Varenykivs'kyi Ln, 14","Kharkiv,Dmytra Vyshnevetskoho St, 56","Kharkiv,Barykadnyi Ln, 2","Kharkiv,Alushtyns'ka St, 3","Kharkiv,Novocherkas'kyi Ln, 12","Kharkiv,Peremozhtsiv Ln, 14","Kharkiv,Poltavskyi Shliakh St, 188-10","Poltavskyi Shliakh St, 144"};
    final static String [] phone = {"+38(099)274-74-56", "+38(099)625-67-75", "+38(099)235-53-60", "+38(099)537-91-63", "+38(099)380-83-79","+38(073)438-40-20","+38(073)032-66-28", "+38(073)795-32-51", "+38(098)460-68-29", "+38(098)933-46-67", "+38(098)417-78-86", "+38(098)977-87-73"};
    final static String[] faculty = {"PM","IT"};
    final static String[] course = {"I", "III", "II", "I", "IV", "II", "III", "V","II","III","I","II"};
    final static String[] group = {"QA","AT"};


    public static List<Students> findAll () {
        List <Students> students = List.of(
                new Students(id[0],lastName[0],firstName[0],patronymic[0],yearOfBirth[0],address[0],phone[0],faculty[0],course[0],group[0]),
                new Students(id[1],lastName[1],firstName[1],patronymic[1],yearOfBirth[1],address[1],phone[1],faculty[1],course[1],group[1]),
                new Students(id[2],lastName[2],firstName[2],patronymic[2],yearOfBirth[2],address[2],phone[2],faculty[1],course[2],group[0]),
                new Students(id[3],lastName[3],firstName[3],patronymic[3],yearOfBirth[3],address[3],phone[3],faculty[1],course[3],group[1]),
                new Students(id[4],lastName[4],firstName[4],patronymic[4],yearOfBirth[4],address[4],phone[4],faculty[0],course[4],group[0]),
                new Students(id[5],lastName[5],firstName[5],patronymic[5],yearOfBirth[5],address[5],phone[5],faculty[0],course[5],group[0]),
                new Students(id[6],lastName[6],firstName[6],patronymic[6],yearOfBirth[6],address[6],phone[6],faculty[1],course[6],group[1]),
                new Students(id[7],lastName[7],firstName[7],patronymic[7],yearOfBirth[7],address[7],phone[7],faculty[1],course[7],group[0]),
                new Students(id[8],lastName[8],firstName[8],patronymic[8],yearOfBirth[8],address[8],phone[8],faculty[0],course[8],group[1]),
                new Students(id[9],lastName[9],firstName[9],patronymic[9],yearOfBirth[9],address[9],phone[9],faculty[1],course[9],group[1])
        );
        return students;
    }

    public static List <Students> getFaculty (List<Students> list, String facult) {
        List <Students> str = list.stream().filter(e->e.getFaculty().equals(facult)).collect(Collectors.toList());
        return str;
    }

    public static List <Students> getFacultyAndCourse (List<Students> list, String facult,String course) {
        List <Students> str = list.stream()
                .filter((e)->e.getFaculty().equals(facult)).filter(e ->e.getCourse().equals(course))
                .collect(Collectors.toList());
        return str;
    }

    public static List <Students> byYear (List<Students> list, int year) {
        List <Students> str = list.stream()
                .filter(e -> e.getYearOfBirth() > year)
                .collect(Collectors.toList());
        return str;
    }

    public static List<String> students (List<Students> list) {

        List<String> rst = list.stream()
                .map((s)->{
                    return "ID: " + s.getId() + ", Student: " + s.getFirstName() +  " " + s.getPatronymic()+ " " + s.getLastName()  + "\n";
                }).collect(Collectors.toList());
        return rst;
    }

    public static List<String> toFaulty (List<Students> list, String toFacolty) {

        List<String> rst = list.stream()
                .map((s)->{
                    s.setFaculty(toFacolty);
                    return "ID: " + s.getId() + ", Faculty: " + s.getFaculty() +  ", Student: " + s.getFirstName() +  " " + s.getPatronymic()+ " " + s.getLastName()  + "\n";
                }).collect(Collectors.toList());
        return rst;
    }

    public static List<String> toGroup (List<Students> list, String toGroup) {

        List<String> rst = list.stream()
                .map((s)->{
                    s.setGroup(toGroup);
                    return "ID: " + s.getId() + ", Group: " + s.getGroup() +  ", Student: " + s.getFirstName() +  " " + s.getPatronymic()+ " " + s.getLastName()  + "\n";
                }).collect(Collectors.toList());
        return rst;
    }

    public static Optional<String> getReduce (List<Students> list, long limit, String separator ) {
        List<String> rst = list.stream()
                .map((s)->{
                    return "ID: " + s.getId()  +  ", Student: " + s.getFirstName() +  " " + s.getPatronymic()+ " " + s.getLastName() + ", Faculty: " + s.getFaculty() + ", Group: " + s.getGroup() + "\n";
                }).collect(Collectors.toList());
        Optional<String> res = rst.stream().limit(limit).reduce((x, y)-> x + separator + y );
        return res;
    }


}
