import lombok.*;

import java.util.*;
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Getter
@Setter
public class Invalid_Pet {
    private String id;
    private Category category;
    private String name;
    private List<String> photoUrls;
    private List<Category> tags;
    private String status;

    public void setId(String id) {
        this.id = id;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Pet setName(String name) {
        this.name = name;
        return null;
    }

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public void setTags(List<Category> tags) {
        this.tags = tags;
    }

    public Pet setStatus(String status) {
        this.status = status;
        return null;
    }

}
