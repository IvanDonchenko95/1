import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Category {

    private int id;
    private String name;

}
