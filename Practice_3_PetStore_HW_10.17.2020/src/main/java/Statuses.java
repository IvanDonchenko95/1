import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Statuses {


    private String available = "available";
    private String pending = "pending";
    private String sold = "sold";


}
