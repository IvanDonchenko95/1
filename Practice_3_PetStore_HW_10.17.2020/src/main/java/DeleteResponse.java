import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteResponse {

    private int code;
    private String type;
    private String message;

}
