import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.messages.JsonSchemaValidationBundle;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import lombok.ToString;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.util.Arrays;
import java.util.Collections;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;


@ToString
public class PetStoreTests {

    @Before
        public void setUp () {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @Test
    public void test1_newPet_Invalid_ID() {
        System.out.println("Test 1 " + "\n");
// Precondition
        Category cat = new Category(1,"Cat");
        Category patrol = new Category(40,"Patrol_1");

        Invalid_Pet newPet = new Invalid_Pet(
                "12345678901234567890",
                cat,
                "Mad",
                Collections.singletonList("urls"),
                Arrays.asList(patrol),
                new Statuses().getAvailable());
        // Test
        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(newPet)
                .post();

        Assertions.assertEquals(500, responseAddPet.getStatusCode());
        System.out.println("Response for adding a new pet: \n" + responseAddPet.asString() + "\n"); // log info
    }

    @Test
    public void test2_updatePet_By_ID() {
        System.out.println("Test 2 " + "\n");
// Precondition
        Category cat = new Category(1,"Cat");
        Category patrol = new Category(40,"Patrol_1");

        Pet newPet = new Pet(
                (long) (1000000000000000001L + (Math.random() * 99999999999999999L)),
                cat,
                "Mad",
                Collections.singletonList("urls"),
                Arrays.asList(patrol),
                new Statuses().getAvailable());

        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(newPet)
                .post();

        System.out.println("Response for adding a new pet: \n" + responseAddPet.asString()); // log info
        System.out.println("Response status: " + responseAddPet.getStatusCode() + "\n");
        Assertions.assertEquals(200,responseAddPet.getStatusCode());
// Test
        Pet updatePet = new Pet(
                newPet.getId(),
                cat,
                "NotMad",
                Collections.singletonList("urls"),
                Arrays.asList(patrol),
                new Statuses().getPending());

        Response responseUpdatePet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(updatePet)
                .put();

        Assertions.assertEquals(200, responseUpdatePet.getStatusCode());
        Pet addedPet = responseUpdatePet.as(Pet.class);

        Pet foundPetById = given()
                .basePath("/pet/" + addedPet.getId())
                .accept("application/json")
                .when()
                .get()
                .as(Pet.class);

        System.out.println("Response for updating pet by ID: \n" + responseUpdatePet.asString() + "\n"); // log info
        Assertions.assertEquals("NotMad", foundPetById.getName());

        Response deleteResponse =
                given()
                        .pathParam("Id", addedPet.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .delete();

        System.out.println("Delete pet: " + deleteResponse.asString() + "\n");

        DeleteResponse deleteResponseAsClass = deleteResponse.as(DeleteResponse.class);

        Assertions.assertEquals(200, deleteResponseAsClass.getCode());
        Assertions.assertNotNull(deleteResponseAsClass.getType());
        Assertions.assertEquals(addedPet.getId(), Long.parseLong(deleteResponseAsClass.getMessage()));
        Response deleteResponse1 =
                given()
                        .pathParam("Id", addedPet.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .get();
        System.out.println("Find deleted pet by ID: " + deleteResponse1.asString());
    }

    @Test
    public void test3_newUser() {
        System.out.println("Test 3 " + "\n");
// Precondition
        User newUser = new User(
                (int) (100 + (Math.random() * 100)),
                "Name_Test_User12",
                "FirstName_Test_User12",
                "LastName_Test_User12",
                "Test_Email@test12.com",
                "Password_Test12",
                "Phone_Test12",
                1);
        // Test
        User responseAddNewUser = given()
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(newUser)
                .post()
                .as(User.class);

        given()
                .basePath("/user/" + newUser.getId())
                .accept("application/json")
                .get()
                .then()
                .assertThat()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("C:\\Users\\Ivan\\IdeaProjects\\Practice3PetStore\\src\\test\\JSONSchema\\schema.json"));
//        System.out.println("Response for getting pet by Id: \n" + foundPetById.toString()); // log info

// Post Condition
        Response deleteResponse =
                given()
                        .pathParam("Id", newUser.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .delete();

        System.out.println(deleteResponse.asString());

        Response deleteResponse1 =
                given()
                        .pathParam("Id", newUser.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .get();

        System.out.println(deleteResponse1.asString());
    }

    @Test
    public void test4_findDeleted_Pet_By_ID() {
        System.out.println("Test 4 " + "\n");
// Precondition
        Category cat = new Category(1,"Cat");
        Category patrol = new Category(40,"Patrol_1");

        Pet newPet = new Pet(
                (long) (1000000000000000001L + (Math.random() * 99999999999999999L)),
                cat,
                "Mad",
                Collections.singletonList("urls"),
                Arrays.asList(patrol),
                new Statuses().getAvailable());
        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(newPet)
                .post();
        System.out.println("Response for adding a new pet: \n" + responseAddPet.asString()); // log info
        System.out.println("Response status: " + responseAddPet.getStatusCode() + "\n");
        Assertions.assertEquals(200,responseAddPet.getStatusCode());
// Test
        Response deleteResponse =
                given()
                        .pathParam("Id", newPet.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .delete();

        System.out.println("Delete pet: " + deleteResponse.asString());
        System.out.println("Response status: " + deleteResponse.getStatusCode() + "\n");
        Assertions.assertEquals(200,deleteResponse.getStatusCode());

        Response deleteResponse1 =
                given()
                        .pathParam("Id", newPet.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .get();

        System.out.println("Find deleted pet by ID: " + deleteResponse1.asString());
        System.out.println("Response status: " + deleteResponse1.getStatusCode());
        Assertions.assertEquals(404, deleteResponse1.getStatusCode());
    }

    @Test
    public void test5_findSold_Pet_By_Status() {
        System.out.println("Test 5 " + "\n");
        // Precondition

        Category cat = new Category(1,"Cat");
        Category patrol = new Category(40,"Patrol_1");

        Pet newPet = new Pet(
                (long) (1000000000000000001L + (Math.random() * 99999999999999999L)),
                cat,
                "Mad",
                Collections.singletonList("urls"),
                Arrays.asList(patrol),
                new Statuses().getSold());

        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(newPet)
                .post();

        System.out.println("Response for adding a new pet: \n" + responseAddPet.asString()); // log info
        System.out.println("Response status: " + responseAddPet.getStatusCode() + "\n");
        Assertions.assertEquals(200,responseAddPet.getStatusCode());

        Response responseAddPet1 = given()
                .basePath("/pet/" + newPet.getId())
                .contentType(ContentType.JSON)
                .when()
                .get();
        Pet pet = responseAddPet1.as(Pet.class);
        System.out.println("Added pet: " + pet.toString());

        // Test
        Pet [] findByStatus = given()
                .when()
                .get("/pet/findByStatus?status=sold")
                .as(Pet [].class);

        for (int i = 0; i < findByStatus.length ; i++) {
            if (findByStatus[i].getId() == newPet.getId()) {
                Assertions.assertEquals(findByStatus[i].getName(), newPet.getName());
                System.out.println("Find It - " + findByStatus[i].getId() + "\n");
            } else {
                continue;
            }
        }
        //Post Conditions

        Response deleteResponse =
                given()
                        .pathParam("Id", newPet.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .delete();

        System.out.println("Delete pet: " + deleteResponse.asString());
        System.out.println("Response status: " + deleteResponse.getStatusCode() + "\n");
        Assertions.assertEquals(200,deleteResponse.getStatusCode());

        Response deleteResponse1 =
                given()
                        .pathParam("Id", newPet.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .get();

        System.out.println("Pet with Id < " + newPet.getId() +  " > not found." + "\n" + deleteResponse.asString());
        System.out.println("Response status: " + deleteResponse1.getStatusCode());
        Assertions.assertEquals(404, deleteResponse1.getStatusCode());
    }

    @Test
    public void test6_newUser_ID_eMail() {
        System.out.println("Test 6 " + "\n");
// Precondition
        User newUser = new User(
                (long) (10000 + (Math.random() * 10000)),
                "Name_Test_User12" + RandomStringUtils.randomAlphabetic(5),
                "FirstName_Test_User12" + RandomStringUtils.randomAlphabetic(5),
                "LastName_Test_User12" + RandomStringUtils.randomAlphabetic(5),
                "Test_Email"+RandomStringUtils.randomAlphabetic(5)+"@test12.com",
                "Password_Test12" + RandomStringUtils.randomAlphabetic(5),
                "Phone_Test12",
                32);

        System.out.println(newUser.toString());

        // Test
        Response responseAddNewUser = given()
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(newUser)
                .post();

        System.out.println(responseAddNewUser.asString());
        System.out.println(responseAddNewUser.asString() + "\n" + responseAddNewUser.getStatusCode());

        Response responseGetName = given()
                .basePath("/user/" + newUser.getUserName())
                .accept("application/json")
                .when()
                .get();
        User getUser = responseGetName.as(User.class);
        System.out.println(responseGetName.asString());
        Assertions.assertEquals(getUser.getEmail(),newUser.getEmail());

//        given()
//                .basePath("/user/" + newUser.getId())
//                .accept("application/json")
//                .get()
//                .then()
//                .assertThat()
//                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("C:\\Users\\Ivan\\IdeaProjects\\Practice3PetStore\\src\\test\\JSONSchema\\schema.json"));
//        System.out.println("Response for getting pet by Id: \n" + foundPetById.toString()); // log info

// Post Condition
        Response deleteResponse =
                given()
                        .pathParam("Id", newUser.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .delete();

        System.out.println(deleteResponse.asString());

        Response deleteResponse1 =
                given()
                        .pathParam("Id", newUser.getId())
                        .basePath("/pet/{Id}")
                        .accept("application/json")
                        .when()
                        .get();

        System.out.println(deleteResponse1.asString());
    }

    @After
        public void exit () {}

}
