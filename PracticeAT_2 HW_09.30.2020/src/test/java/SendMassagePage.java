import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter

public class SendMassagePage {

    WebDriver driver;

    public SendMassagePage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = "#send_to")
    private WebElement setEmail;
    @FindBy (css = "#composeareacontainer [id=\"body\"]")
    private WebElement write_In_Text_Field;
    @FindBy (xpath = "//*[@id=\"b3_cnt\"]/div[2]/div[1]/div[1]/input[2]")
    private WebElement sendMassage;
    @FindBy (xpath = "//*[@id=\"b3_cnt\"]/div[1]/center[2]/div")
    private WebElement worningMessage;

    void setEmail (String email) throws InterruptedException {
        setEmail.sendKeys(email);
        Thread.sleep(500);
    }
    void write_In_Text_Field (String text) throws InterruptedException {
        write_In_Text_Field.sendKeys(text);
        Thread.sleep(500);
    }
    void sendMassage () {
        sendMassage.click();
    }


}
