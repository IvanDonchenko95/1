import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmailPage {

    public EmailPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    @FindBy (css = "#id_send_email")
    private WebElement sendMail;
    @FindBy (css = "[title=\"Спам\"]")
    private WebElement spam;
    @FindBy (css = "[title=\"Вхідні\"]")
    private WebElement icomingMessages;
    @FindBy (css = "[title=\"Кошик\"]")
    private WebElement deleted;
    @FindBy (css = "#id_delete1 [class=\"panel_text\"]")
    private WebElement deleteButton;
    @FindBy (css = "[id=\"b3_cnt\"]")
    private WebElement findMassage;
    @FindBy (css = "#id_spam1 [class=\"panel_text\"]")
    private WebElement its_a_Spam;
    @FindBy (css = "[class=\"treeContainer1\"]:nth-child(1) [class=\"treeElement grayFon\"]") //
    private WebElement numOfSendMassage;
    @FindBy (xpath = "//*[@id=\"left\"]/div[1]/div[3]/div[4]/div/div[1]") //
    private WebElement numOfSpamMassage;

    void sendMail () throws InterruptedException {
       sendMail.click();
        Thread.sleep(1000);
    }
    void spam () throws InterruptedException {
       spam.click();
        Thread.sleep(1000);
    }
    void icomingMessages () throws InterruptedException {
        icomingMessages.click();
        Thread.sleep(1000);
    }
    void deleted () throws InterruptedException {
        deleted.click();
        Thread.sleep(1000);
    }
    void deleteButton() {
        deleteButton.click();
    }
    void findMassage (String name) throws InterruptedException {
       WebElement a = findMassage;
        Thread.sleep(1000);
        a.findElement(new By.ByLinkText(name)).click();
    }
    void its_a_Spam () throws InterruptedException {
       its_a_Spam.click();
        Thread.sleep(500);
    }

    public int getSendedMassages () {
        int num = Integer.valueOf(numOfSendMassage.getText());
        return num;
    }

    public int getNumOfSpamMassage () {
        int num = Integer.valueOf(numOfSpamMassage.getText());
        return num;
    }

}
