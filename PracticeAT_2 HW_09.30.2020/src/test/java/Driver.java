import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
@Getter
@Setter
public class Driver {

    private static Driver instance = null;
    private static WebDriver driver;

   private Driver() {}

    public static Driver getInstance() {
        if (instance == null) {
            instance = new Driver();
        }
        return instance;
    }

    public static WebDriver getDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new  ChromeDriver();
            return driver;
    }


}
