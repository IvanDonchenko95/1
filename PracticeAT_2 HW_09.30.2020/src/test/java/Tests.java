import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

public class Tests {

    private PagesInformation inf = new PagesInformation();
    private Driver instance = Driver.getInstance();
    private WebDriver driver = instance.getDriver();
    private AuthorizationPage authPage = new AuthorizationPage(driver);
    private EmailPage emailPage = new EmailPage(driver);
    private SendMassagePage send = new SendMassagePage(driver);
    private SetingsPage settings = new SetingsPage(driver);

    @BeforeEach
    public void open () {
        driver.get(inf.getUrl());
    }

    @Test
    public void test1_Get_User_eMailName () throws InterruptedException {
    authPage.enterInfo(inf.getLogin(),inf.getPassword());
    authPage.clickEnterButton();
    Thread.sleep(1000);

    String actual = authPage.getUserMail();
    String expected = inf.getEmail();
    Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test2_Count_Incoming_Massage () throws InterruptedException {
        // Precondition
        authPage.enterInfo(inf.getLogin(),inf.getPassword());
        authPage.clickEnterButton();
        // Steps
        int atFirstTime = emailPage.getSendedMassages();
        emailPage.sendMail();
        send.setEmail(inf.getEmail());
        send.write_In_Text_Field(inf.getTexst());
        send.sendMassage();
        emailPage.findMassage("Don Ivan");
        int actual = emailPage.getSendedMassages();
        emailPage.deleteButton();
        // Post Condition
        emailPage.deleted();
        emailPage.findMassage("Don Ivan");
        emailPage.deleteButton();

        int expected = atFirstTime + 1;
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test3_Count_Spam_Massage () throws InterruptedException {
        // Precondition
        authPage.enterInfo(inf.getLogin(),inf.getPassword());
        authPage.clickEnterButton();
        int atFirstTime = emailPage.getNumOfSpamMassage();
        // Steps
        emailPage.sendMail();
        send.setEmail(inf.getEmail());
        send.write_In_Text_Field(inf.getTexst());
        send.sendMassage();
        emailPage.findMassage("Don Ivan");
        emailPage.its_a_Spam();
        int actual = emailPage.getNumOfSpamMassage();
        //Post Condition
        emailPage.spam();
        emailPage.findMassage("Don Ivan");
        emailPage.deleteButton();
        settings.setings();
        settings.black_White_List();
        settings.delete_from_black_list();

        int expected = atFirstTime + 1;
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test4_Wrong_Email () throws InterruptedException {
        authPage.enterInfo("Qwqwewqr",inf.getPassword());
        authPage.clickEnterButton();
        Thread.sleep(1000);

        String actual = authPage.getWorningMassage().getText();
        String expected = "Введено невірний пароль";
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test5_Wrong_Pass () throws InterruptedException {
        authPage.enterInfo(inf.getLogin(), "wgqsdgsaDGH");
        authPage.clickEnterButton();
        Thread.sleep(1000);

        String actual = authPage.getWorningMassage().getText();
        String expected = "Введено невірний пароль";
        Assertions.assertEquals(actual,expected);
    }

    @Test
    public void test6_Send_Empty_Massage () throws InterruptedException {
        // Precondition
        authPage.enterInfo(inf.getLogin(),inf.getPassword());
        authPage.clickEnterButton();
        // Steps
        emailPage.sendMail();
        send.setEmail("");
        send.write_In_Text_Field("");
        send.sendMassage();

        String actual = send.getWorningMessage().getText();
        Assertions.assertEquals("Ви не заповнили поле \"Кому:\"",actual);
    }

    @AfterEach
    public void close () {
       driver.close();
    }

}
