import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class AuthorizationPage {

    public AuthorizationPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    @FindBy (css = "#login-field")
    private WebElement loginInput;
    @FindBy (css = "#pass-field")
    private WebElement passwordInput;
    @FindBy (css = "#loginbtnua")
    private WebElement inputButton;
    @FindBy (css = "[style=\"float:left;\"] strong")
    private WebElement userMail;
    @FindBy (xpath = "//*[@id=\"loginForm\"]/label[2]/strong/font")
    private WebElement worningMassage;


    void enterInfo (String login, String password) {
        loginInput.sendKeys(login);
        passwordInput.sendKeys(password);
    }

    void clickEnterButton () {
        inputButton.click();
    }

    public String getUserMail () {
        return userMail.getText();
    }
}
