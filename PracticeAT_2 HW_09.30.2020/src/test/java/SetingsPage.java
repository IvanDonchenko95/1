import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SetingsPage {

    public SetingsPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = "#id_settings")
    private WebElement setings;
    @FindBy (css = "[title=\"Управління чорним/бiлим списками\"]")
    private WebElement black_White_List;
    @FindBy (css = "#blacklist")
    private WebElement delete_from_black_list;
    @FindBy (css = "[class=\"btn_new\"]:nth-child(1)")
    private WebElement save;

    void setings () throws InterruptedException {
        setings.click();
        Thread.sleep(500);
    }
    void black_White_List () throws InterruptedException {
        black_White_List.click();
        Thread.sleep(500);
    }
    void delete_from_black_list () throws InterruptedException {
        delete_from_black_list.clear();
        Thread.sleep(500);
        save.click();
    }

}
