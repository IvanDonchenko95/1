package com.Practice;

public abstract class CommonData {

    int id;

    CommonData () {};

    public CommonData (int id) {
        this.id = id;
    }

    public int getId () {
        return  id;
    }

}
