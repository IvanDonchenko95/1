package com.Practice;

public class User extends CommonData{

private  String name;

public  User (int id , String name) {

    super(id);
    this.name = name;

}

public String getName () {
    return  name;
}

@Override
    public int getId () {
    return super.getId();
}

}
