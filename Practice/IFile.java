package com.Practice;

import java.util.ArrayList;

public interface IFile extends iCrud {

    void write (Note note);
    void read();
    void openStream();
    void closeStream();

}
