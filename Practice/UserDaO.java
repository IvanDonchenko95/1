package com.Practice;

public class UserDaO implements IDataBase {

    @Override
    public void insert(User user) {
        System.out.println("I insert");
    }

    @Override
    public void selectAll() {
        System.out.println("I select All users");
    }

    @Override
    public void connect() {
        System.out.println("I connect");
    }

    @Override
    public void disconnect() {
        System.out.println("I disconnect");
    }

    @Override
    public void update(CommonData data) {
        System.out.println(data + " I disconnect");
    }

    @Override
    public void delete(int id) {
        System.out.println( "I was deleted " + id);
    }

    @Override
    public void clearAll() {
        System.out.println( "I was clear All ");
    }
}
