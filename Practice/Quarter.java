package com.Practice;

import java.util.LinkedList;

public class Quarter {
    private static LinkedList<Data> queue = new LinkedList<>();
    public static int conter = 0;
    private static Quarter instance = null;
    private Quarter () {
        conter++;
    }

    public static Quarter  getInstance() {
        if (instance == null) {
            synchronized (queue) {
                if (instance == null) {
                    instance = new Quarter();
                }
            }
        }
        return instance;
    }
}

