package com.Practice;

public class Producer2 extends Thread{

    private final Data data;
    private final int count;

    public Producer2(Data data, int count) {
        this.data = data;
        this.count = count;
    }
    @Override
    public void run() {
        try {
            for (int i = 0; i < count; i++) {
                System.out.println("Producer 2 put Data - " + (i+1));
                data.put("produced - " + i);
                Thread.sleep(1000);

            }
        } catch (Exception e) {

        }
        System.out.println("Producer 2 thread finished");
    }

}
