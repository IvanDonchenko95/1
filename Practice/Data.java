package com.Practice;
import java.util.*;

public class Data {

    private final LinkedList<String> data = new LinkedList<>();

    public String get() {

        synchronized (data) {
            try {
                if (data.size() == 0) {
                    data.wait(1500);
                }
            } catch (Exception e) {
                System.out.println("Failure");
            }
            return data.poll();
        }
    }

    public void put(String s) {

        synchronized (data) {
            data.add(s);
            data.notify();
        }
    }


}
