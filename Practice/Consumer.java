package com.Practice;

public class Consumer extends Thread{

    final int CHECK_TIME = 2;
    final Data data;
    int chek = 0;

    public Consumer(Data data) {
        this.data = data;
    }

    @Override
    public void run() {
        String s;
        while (chek < CHECK_TIME) {
            s = data.get();
            if (s == null) {
                chek++;
                System.out.println("Consumer timeout: " + chek);
            } else {
                System.out.println("Consumer get Data : " + s);
                chek = 0;
            }
        }
        System.out.println("Consumer thread finished");
    }

}
