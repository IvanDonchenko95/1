package com.Practice;

public class Producer extends Thread{
    private final Data data;
    private final int count;

    public Producer(Data data, int count) {
        this.data = data;
        this.count = count;
    }

    @Override
    public void run() {

        try {
            for (int i = 0; i < count; i++) {
                System.out.println("Producer 1 put Data - " + i);
                data.put("produced - " + i);
                Thread.sleep(1000);
            }
        } catch (Exception e) {

        }

        System.out.println("Producer 1 thread finished");
    }

}
