package com.Practice;

public interface iCrud  {

    void update(CommonData data);
    void delete(int id);
    void clearAll();

}
