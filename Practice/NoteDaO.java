package com.Practice;

import java.util.ArrayList;

public class NoteDaO implements IFile {


    @Override
    public void write(Note note) {
            System.out.println("Write" + note);
    }

    @Override
    public void read() {
        System.out.println("Read");
    }

    @Override
    public void openStream() {
        System.out.println("Open Stream");
    }

    @Override
    public void closeStream() {
        System.out.println("Close Stream");
    }

    @Override
    public void update(CommonData data) {
        System.out.println("Note Update");
    }

    @Override
    public void delete(int id) {
        System.out.println("Note Delete");
    }

    @Override
    public void clearAll() {
        System.out.println("Note Clear All");
    }
}
