package com.Practice;

public class Main {

    public static void main(String[] args) {

        User user=new User(1, "Yuliana");
        user.getName();
        UserDaO userDaO=new UserDaO();
        userDaO.connect();
        userDaO.insert(user);
        userDaO.delete(user.getId());
        userDaO.selectAll();
        userDaO.update(user);
        userDaO.clearAll();
        userDaO.disconnect();

        Note note=new Note(2, "Message");
        note.getMessage();
        NoteDaO noteDaO = new NoteDaO();
        noteDaO.openStream();
        noteDaO.read();
        noteDaO.update(note);
        noteDaO.write(note);
        noteDaO.clearAll();
        noteDaO.closeStream();


    }
}
