package com.Practice;

public interface IDataBase extends iCrud {

    void insert(User user);
    void selectAll();
    void connect();
    void disconnect();

}
